import { createDrawerNavigator } from "@react-navigation/drawer";
import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import TabBarIcon from "../components/TabBarIcon";
import HomeScreen from "../screens/HomeScreen";
import Auth from "../screens/Auth";
import SelectStore from "../screens/SelectStore";
import Results from "../screens/Results";
import Cart from "../screens/Cart";
import Products from "../screens/Products";
import Checkout from "../screens/Checkout";
import GetProducts from "../screens/GetProducts";
import DrawerScreen from "../screens/DrawerScreen";
import Orders from "../screens/Orders";
import Profile from "../screens/Profile";
import axios from "axios";
import Payment from "../screens/Payment";
import PaymentPage from "../screens/PaymentPage";
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default class App extends React.Component {
  render() {
    return (
      <Drawer.Navigator
        initialRouteName="Home"
        drawerContent={(props) => DrawerScreen(props)}
      >
        <Stack.Screen name="Home" component={HomeStack} />
        <Drawer.Screen name="Auth" component={Auth} />
      </Drawer.Navigator>
    );
  }
}

class HomeStack extends React.Component {
  render() {
    return (
      <Stack.Navigator headerMode={null}>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="GetProducts" component={GetProducts} />
        <Stack.Screen name="SelectStore" component={SelectStore} />
        <Stack.Screen name="Results" component={Results} />
        <Stack.Screen name="Cart" component={CartStack} />
        <Stack.Screen name="Products" component={Products} />
        <Stack.Screen name="Checkout" component={CheckoutStack} />
        <Stack.Screen name="Orders" component={Orders} />
        <Stack.Screen name="Profile" component={Profile} />
      </Stack.Navigator>
    );
  }
}

class CheckoutStack extends React.Component {
  render() {
    return (
      <Stack.Navigator headerMode={null}>
        <Stack.Screen name="Checkout" component={Checkout} />
        <Stack.Screen name="Payment" component={Payment} />
        <Stack.Screen name="PaymentPage" component={PaymentPage} />
      </Stack.Navigator>
      
    );
  }
}

  class CartStack extends React.Component {
    render() {
      return (
        <Stack.Navigator headerMode={null}>
          <Stack.Screen name="Cart" component={Cart} />
          <Stack.Screen name="PaymentPage" component={PaymentPage} />
        </Stack.Navigator>
        
      );
    }
}
