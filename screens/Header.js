import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  AsyncStorage,
  Platform,
  TouchableOpacity,
} from "react-native";
import { Toast } from "native-base";
import {
  EvilIcons,
  AntDesign,
  Entypo,
  Feather,
  SimpleLineIcons,
} from "@expo/vector-icons";
import axios from "axios";
import {
  userData,
  cartCount,
  appContent,
  appContentType,
} from "../redux-store/action";
import { connect } from "react-redux";
import English from "../constants/Languages/English";
import Arabic from "../constants/Languages/Arabic";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      askAuth: false,
      searchText: "",
      userData: null,
      logo: "",
      count: "",
    };
  }

  async componentDidMount() {
    await this.getUser();
    this.getCart();
    this.focusListener = this.props.navigation.addListener("focus", () => {
      this.getCart();
    });
  }

  getUser = async () => {
    try {
      const value = await AsyncStorage.getItem("userData");
      if (value !== null) {
        this.setState({ userData: JSON.parse(value) });
        await this.props.dispatch(userData(JSON.parse(value)));
      } else {
        this.setState({ count: "" });
      }
    } catch (error) {
      console.log(error);
    }
  };

  logout = async () => {
    await AsyncStorage.removeItem("userData");
    await this.props.dispatch(userData(null));
    Toast.show({
      text: "User logged out",
      buttonText: "Okay",
      duration: 3000,
    });
    await this.setState({ userData: null });
    this.setState({ askAuth: false });
  };

  getCart = async () => {
    var self = this;
    if (self.props.userData) {
      cart = [];
      try {
        cart = await AsyncStorage.getItem("cart");
        if (cart == null) {
          cart = [];
          await self.props.dispatch(cartCount(""));
        } else {
          cart = await JSON.parse(cart);
        }
        console.log(cart);
      } catch (error) {
        console.log(error);
        await self.props.dispatch(cartCount(""));
      }
      if (cart?.length > 0) {
        await this.props.dispatch(cartCount(cart.length.toString()));
      } else {
        await self.props.dispatch(cartCount(""));
      }
    } else {
      await self.props.dispatch(cartCount(""));
    }
  };

  render() {
    return (
      <View>
        <View
          style={{
            backgroundColor: "#fff",
            flexDirection: "row",
            alignItems: "center",
            height: 100,
            paddingTop: Platform.OS === "android" ? 16 : 0,
          }}
        >
          <View
            style={{
             
              flexDirection: "row",
              alignItems: "center",
              height: "100%",
              marginLeft: 12,
            }}
          >
            {this.props.goBack ? (
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <AntDesign
                  name="arrowleft"
                  style={{ fontSize: 30, color: "#000000" }}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <EvilIcons
                  name="navicon"
                  style={{ fontSize: 30, color: "#000000" }}
                />
              </TouchableOpacity>
            )}
          </View>
          <View style={{ flex: 1, alignItems:"center" }}>
            <TouchableOpacity
              onPress={async () => {
                this.props.navigation.navigate("Home");
              }}
              style={{marginRight: this.props.userData? -48 : -36}}
            >
              <Image
                source={require("../assets/images/hadoro_main_icon.png")}
                style={{ height: 100, width: 100 }}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-end",
              height: "100%",
              marginRight: 12,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.getUser();
                this.setState({ askAuth: !this.state.askAuth });
              }}
            >
              <AntDesign
                name="user"
                style={{ fontSize: 24, color: "#000000" }}
              />
            </TouchableOpacity>
            {this.props.userData != null ? (
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Cart")}
                style={{ marginLeft: 12 }}
              >
                <SimpleLineIcons
                  name="bag"
                  style={{ fontSize: 24, color: "#000000" }}
                />
                <Text
                  style={{
                    position: "absolute",
                    marginLeft: 9,
                    fontSize: 13,
                    marginTop: Platform.OS === "android" ? 5 : 7,
                    color: "#000000",
                  }}
                >
                  {this.props.cartCount}
                </Text>
              </TouchableOpacity>
            ) : null}
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Results")}
              style={{ marginLeft: 12 }}
            >
              <AntDesign
                name="search1"
                style={{ fontSize: 22, color: "#000000" }}
              />
            </TouchableOpacity>
          </View>
        </View>

        {this.state.askAuth ? (
          <View>
            <View
              style={{ flexDirection: "row", backgroundColor: "#80808010" }}
            >
              <Text style={{ padding: 12, fontSize: 12, fontWeight: "bold" }}>
                {this.props.userData != null
                  ? this.props.appContent.hi_user +
                    " " +
                    this.props.userData.first_name.toUpperCase()
                  : this.props.appContent.welcome}
              </Text>
            </View>
            {this.props.userData != null ? (
              <View
                style={{
                  backgroundColor: "#ffffff",
                  padding: 24,
                  height: 200,
                  shadowColor: "grey",
                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowRadius: 5,
                  shadowOpacity: 1.0,
                }}
              >
                <View
                  style={{
                    paddingTop: 8,
                    paddingBottom: 16,
                    borderBottomColor: "#80808040",
                    borderBottomWidth: 2,
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("Profile")}
                  >
                    <Text style={{ fontSize: 18 }}>
                      {this.props.appContent.update_personal_details}
                    </Text>
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    paddingTop: 16,
                    paddingBottom: 16,
                    borderBottomColor: "#80808040",
                    borderBottomWidth: 2,
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("Orders");
                      this.setState({ askAuth: !this.state.askAuth });
                    }}
                  >
                    <Text style={{ fontSize: 18 }}>
                      {this.props.appContent.view_orders}
                    </Text>
                  </TouchableOpacity>
                </View>
                {/* <View
                  style={{
                    paddingTop: 16,
                    paddingBottom: 16,
                    borderBottomColor: "#80808040",
                    borderBottomWidth: 2,
                  }}
                >
                  <TouchableOpacity>
                    <Text style={{ fontSize: 18 }}>
                      Manage and edit delivery address
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    paddingTop: 16,
                    paddingBottom: 16,
                    borderBottomColor: "#808080",
                    borderBottomWidth: 2,
                  }}
                >
                  <TouchableOpacity>
                    <Text style={{ fontSize: 18 }}>
                      Manage and edit my payment details
                    </Text>
                  </TouchableOpacity>
                </View> */}
                <View
                  style={{
                    marginTop: 22,
                    marginBottom: 12,
                  }}
                >
                  <TouchableOpacity onPress={this.logout}>
                    <Text
                      style={{
                        fontSize: 20,
                        fontWeight: "bold",
                      }}
                    >
                      {this.props.appContent.sign_out}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View
                style={{
                  backgroundColor: "#ffffff",
                  padding: 24,
                  height: 140,
                  shadowColor: "grey",
                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowRadius: 5,
                  shadowOpacity: 1.0,
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Auth");
                    this.setState({ askAuth: false });
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#000000",
                      justifyContent: "center",
                      alignItems: "center",
                      height: 40,
                    }}
                  >
                    <Text style={{ color: "white", fontWeight: "bold" }}>
                      {this.props.appContent.signin}
                    </Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{ marginTop: 12 }}
                  onPress={() => {
                    this.setState({ askAuth: false });
                    this.props.navigation.navigate("Auth");
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "white",
                      borderColor: "black",
                      borderWidth: 1,
                      justifyContent: "center",
                      alignItems: "center",
                      height: 40,
                    }}
                  >
                    <Text style={{ color: "black", fontWeight: "bold" }}>
                      {this.props.appContent.register}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const cartCount = state.cartCount;
  const userData = state.userData;
  const appContent = state.appContent;
  const appContentType = state.appContentType;
  return { cartCount, userData, appContent, appContentType };
};

export default connect(mapStateToProps)(Header);
