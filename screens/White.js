import React, { Component } from "react";
import { View, Image, TextInput, Text, AsyncStorage, Dimensions } from "react-native";

import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import { Picker, Icon, Toast, Spinner } from "native-base";
import axios from "axios";
import Header from "./Header";
import { CommonActions } from "@react-navigation/native";
import country_codes from "../constants/country_codes.json";
import { connect } from "react-redux";
class White extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      first_name: "",
      last_name: "",
      password: "",
      mobile: "",
      confirm_password: "",
      country_initals: "",
      userData: {},
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate("Root");
    }, 5000);
  }

  render() {
    return (
        <View style={{ flex: 1, backgroundColor: "black" }}>
        <Image
          source={require("../assets/images/splash.gif")}
          style={{
            flex: 1,
            justifyContent: "center",
            alignSelf: "center",
            width: "100%",
          }}
        />
      </View>
    );
  }
}

export default White;
