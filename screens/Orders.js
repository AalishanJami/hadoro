import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  Dimensions,
  ScrollView,
  StyleSheet,
  AsyncStorage,
} from "react-native";
import Footer from "./Footer";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import Header from "./Header";
import axios from "axios";
import { func } from "prop-types";
import { Spinner, Toast } from "native-base";
import services from "../services";
class Orders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      userData: {},
      orders: [],
      products: [],
      isEmpty: false,
      isLoading: true,
      imageTemp: {},
    };
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", () => {
     this.getOrder();
    });
  }

  getOrder = async () => {
    this.setState({ isLoading: true });
    var response = await services.restGetHandleParams("orders?customer=" + this.props.userData.id);
    if (response.length > 0) {
      this.setState({ isEmpty: false });
      this.setState({ count: response.length });
      this.setState({ orders: response });
      var res = await this.generateImage(response);
    } else {
      this.setState({ isEmpty: true });
      this.setState({ count: 0 });
      this.setState({ orders: [] });
    }
    this.setState({isLoading: false})
  };

  generateImage = (temp) => {
    var promises = [];
    var imageTemp = {};
    var standard_secrets = "?consumer_key=ck_ecb8e55143d93d279dc599b6b2013b72054576db&consumer_secret=cs_0a6a79f9a8199c4bd5d06c15789ac7e186051c43"
    var x;
    var y;
    temp.forEach((element, i) => {
      element.line_items.forEach((product, pi) => {
        console.log("https://hadoro.ae/wp-json/wc/v3/" + "products/" + product.product_id + "/variations/" + product.variation_id + standard_secrets);
        var promise = axios.get("https://hadoro.ae/wp-json/wc/v3/" + "products/" + product.product_id + "/variations/" + product.variation_id + standard_secrets)
        .then(res=>{
          x = res.data._links.self[0].href.split('/').slice(-1)[0];
          y = res.data._links.self[0].href.split('/').slice(-3)[0]
          x = x.toString();
          y = y.toString();
          console.log(x, y);
          var xObj = imageTemp[x];
          var obj = xObj?.[y] ? xObj?.[y] : {};
          obj[y] = res.data.image.src
          imageTemp[x] = obj;
        })
        .catch(err=>{
          console.log(err)
        })
        promises.push(promise);
      });
    });
    Promise.all(promises)
    .then(()=>{
        console.log(imageTemp)
        this.setState({imageTemp})
    })
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
         <Header navigation={this.props.navigation} goBack={true}/>
        <View>
          <View
            style={{
              height: 50,
              backgroundColor: "#e0e0e0",
              alignItems: "center",
              flexDirection: "row",
            }}
          >
            <Text style={{ marginLeft: 12, fontWeight: "bold", flex: 1 }}>
              {this.state.count} {this.props.appContent.order_placed}
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.getOrder();
              }}
            >
              <EvilIcons
                name="refresh"
                style={{ color: "#000000", marginRight: 12, fontSize: 30 }}
              />
            </TouchableOpacity>
          </View>

          <ScrollView>
            <View style={{ flex: 1, marginBottom: 200 }}>
              {this.state.isLoading ? (
                <Spinner />
              ) : this.state.isEmpty ? (
                <View
                  style={{
                    margin: 12,
                    alignItems: "center",
                    justifyContent: "center",
                    paddingTop: 100,
                    paddingBottom: 100,
                  }}
                >
                  <AntDesign
                    name="shoppingcart"
                    style={{ fontSize: 80, color: "#00000080" }}
                  />
                  <Text style={{ color: "#00000097" }}>
                    NO ORDERS IN YOUR LIST
                  </Text>
                </View>
              ) : (
                this.state.orders.map((ii, x) => (
                  <View
                    style={{
                      margin: 12,
                      borderWidth: 1,
                      borderColor: "#00000040",
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        borderBottomWidth: 1,
                        borderBottomColor: "#00000040",
                        padding: 12,
                      }}
                    >
                      <Text
                        style={{ fontWeight: "bold", fontSize: 18, flex: 1 }}
                      >
                        {this.props.appContent.order_summary}
                      </Text>
                    </View>
                    <View>
                      <View
                        style={{
                          // alignItems: "center",
                          padding: 12,
                          borderBottomWidth: 1,
                          borderBottomColor: "#00000040",
                        }}
                      >
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>
                          {this.props.appContent.customer_details}
                        </Text>
                        <Text style={{ fontSize: 17 }}>
                          {ii.shipping.first_name} {ii.shipping.last_name}
                        </Text>
                        <Text style={{ fontSize: 17 }}>{ii.billing.email}</Text>
                      </View>
                    </View>
                    <View>
                      <View
                        style={{
                          // alignItems: "center",
                          padding: 12,
                          borderBottomWidth: 1,
                          borderBottomColor: "#00000040",
                        }}
                      >
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>
                          {this.props.appContent.billing_shipping_address}
                        </Text>
                        <Text style={{ fontSize: 17 }}>
                          {ii.shipping.first_name} {ii.shipping.last_name}
                        </Text>
                        <Text style={{ fontSize: 17 }}>{ii.shipping.address_1} {ii.shipping.address_2}</Text>
                        <Text style={{ fontSize: 17 }}>{ii.shipping.city}</Text>
                        <Text style={{ fontSize: 17 }}>
                          {ii.shipping.country}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View
                        style={{
                          // alignItems: "center",
                          padding: 12,
                          borderBottomWidth: 1,
                          borderBottomColor: "#00000040",
                        }}
                      >
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>
                          {this.props.appContent.order_details}
                        </Text>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={{ fontSize: 17, flex: 1 }}>
                            {this.props.appContent.order_id}
                          </Text>
                          <Text style={{ flex: 1 }}>{ii.id.toString()}</Text>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={{ fontSize: 17, flex: 1 }}>
                            {this.props.appContent.payment_method}
                          </Text>
                          <Text style={{ flex: 1 }}>Cash on Delivery</Text>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={{ fontSize: 17, flex: 1 }}>
                            {this.props.appContent.order_status}
                          </Text>
                          <Text style={{ flex: 1 }}>
                            {ii.status.toUpperCase()}
                          </Text>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={{ fontSize: 17, flex: 1 }}>
                            {this.props.appContent.order_date}
                          </Text>
                          <Text style={{ flex: 1 }}>
                            {ii.date_created.split("T")[0]}
                          </Text>
                        </View>
                      </View>
                    </View>

                    {ii.line_items.map((item, y) => (
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          padding: 12,
                          marginTop: 12,
                          backgroundColor: "#ffffff70",
                        }}
                      >
                        <Image
                          style={{ height: 140, width: 140 }}
                          source={{ uri: this.state.imageTemp?.[item.variation_id.toString()]?.[item.product_id.toString()] }}
                        ></Image>

                        <View style={{ marginLeft: 12 }}>
                          <View style={{ flex: 1, justifyContent: "center" }}>
                            <Text>
                              <Text>AED</Text>
                              <Text
                                style={{ fontWeight: "bold", fontSize: 20 }}
                              >
                                {item.total.toString()}
                              </Text>
                            </Text>
                            <Text>{item.name.substring(0, 20)}</Text>
                          </View>
                          <View style={{ flexDirection: "row" }}>
                            <Text
                              style={{
                                marginTop: 6,
                                fontSize: 18,
                                marginLeft: 12,
                              }}
                            >
                              {this.props.appContent.quantity}
                            </Text>
                            <View
                              style={{
                                backgroundColor: "#000",
                                height: 35,
                                borderRadius: 17,
                                width: 35,
                                justifyContent: "center",
                                alignItems: "center",
                                marginLeft: 12,
                              }}
                            >
                              <Text style={{ fontSize: 18, color: "#fff" }}>
                                {item.quantity.toString()}
                              </Text>
                            </View>
                          </View>
                        </View>
                        <View></View>
                      </View>
                    ))}
                  </View>
                ))
              )}
            </View>
          </ScrollView>
        </View>
        <Footer navigation={this.props.navigation} />
      </View>
    );
  }
}

import { connect } from "react-redux";
const mapStateToProps = (state) => {
  const appContent = state.appContent;
  const appContentType = state.appContentType;
  const userData = state.userData;

  return { appContent, appContentType, userData };
};

export default connect(mapStateToProps)(Orders);
