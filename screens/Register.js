import React, { Component } from "react";
import { View, Image, TextInput, Text, AsyncStorage } from "react-native";
import services from "../services";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Picker, Icon, Toast, Spinner } from "native-base";
import axios from "axios";
import { CommonActions } from "@react-navigation/native";
import country_codes from "../constants/country_codes.json";
import { connect } from "react-redux";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      first_name: "",
      last_name: "",
      password: "",
      mobile: "",
      confirm_password: "",
      country_initals: "+968",
      isLoading: false
    };
  }

  async componentDidMount() {
    console.log(country_codes);
  }

  validate = async () => {
    if (this.state.password == this.state.confirm_password) {
      if (this.state.password.length >= 6) {
        return true;
      } else {
        Toast.show({
          text: "Please choose a stronger password",
          buttonText: "Okay",
        });
        return false;
      }
    } else {
      Toast.show({
        text: "Password does not match",
        buttonText: "Okay",
      });
      return false;
    }
  };

  register = async () => {
    this.setState({ isLoading: true });
    var response = await services.restPostHandle("customers", {
      email: this.state.email,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      username: this.state.username,
    });
    this.setState({ isLoading: false });
    if (response.code == "registration-error-email-exists") {
      Toast.show({
        text: "An account is already registered with your email address",
        buttonText: "Okay",
      });
    } else if (response.code == "registration-error-username-exists") {
      Toast.show({
        text: "An account is already registered with that username",
        buttonText: "Okay",
      });
    } else if (response.code) {
      Toast.show({
        text: response.code,
        buttonText: "Okay",
      });
    } else {
      await AsyncStorage.setItem("userData", JSON.stringify(response));
      Toast.show({
        text: "Hi, " + this.state.first_name,
        buttonText: "Okay",
        duration: 3000,
      });
      this.props.navigation.navigate("Home");
    }
  };

  render() {
    return (
      <View style={{ width: "100%", padding: 24 }}>
        <Text style={{ marginTop: 24, fontWeight: "bold" }}>
          {this.props.appContent.email}*
        </Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(email) => this.setState({ email })}
          keyboardType="email-address"
          placeholder="abc@exapmple.com"
          value={this.state.email}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>
          {this.props.appContent.first_name}*
        </Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(first_name) => this.setState({ first_name })}
          value={this.state.first_name}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>
          {this.props.appContent.last_name}*
        </Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(last_name) => this.setState({ last_name })}
          value={this.state.last_name}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>Username*</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(username) => this.setState({ username })}
          value={this.state.username}
        />

        <TouchableOpacity
          style={{ marginTop: 24, marginBottom: 24 }}
          onPress={this.register}
        >
          <View
            style={{
              width: "100%",
              backgroundColor: "#000000",
              alignItems: "center",
              justifyContent: "center",
              padding: 12,
            }}
          >
            {this.state.isLoading ? (
              <Spinner color="#fff"/>
            ) : (
              <Text style={{ color: "#fff", fontWeight: "bold" }}>
                {this.props.appContent.register}
              </Text>
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const appContent = state.appContent;
  const appContentType = state.appContentType;

  return { appContent, appContentType };
};

export default connect(mapStateToProps)(Register);
