import * as React from "react";
import {
  View,
  ScrollView,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";
import Header from "./Header";
import { TextInput } from "react-native-gesture-handler";
import Footer from "./Footer";
import Shipping from "./Shipping";
import {
  Spinner,
  Picker,
  Icon,
  ListItem,
  Radio,
  Left,
  Right,
  Toast,
} from "native-base";
import { userData, cartCount } from "../redux-store/action";
import { AntDesign } from "@expo/vector-icons";
import country_codes from "../constants/country_codes.json";
import axios from "axios";

class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      address: "",
      city: "",
      state: "",
      zip: "",
      country: "UAE",
      userData: {},
      orderDetails: {},
      addingToCart: false,
      selected: "shipping",
      hasSavedAddress: false,
      useSaved: false,
      addressData: {},
      address2: "",
      email: "",
      customer_id: "",
      country_array: [],
      countryss: {}
    };
  }

  async componentDidMount() {
    this.getUser();
  }

  getUser = async () => {
    try {
      var value = this.props.userData;
      console.log(value);
      if (value !== null) {
        this.setState({ userData: value });
        this.setState({ first_name: value.first_name });
        this.setState({ last_name: value.last_name });
        this.setState({ first_name: value.first_name });
        this.setState({ address: value.shipping.address_1 });
        this.setState({ address2: value.shipping.address_2 });
        this.setState({ city: value.shipping.city });
        this.setState({ zip: value.shipping.postcode });
        this.setState({ country: value.shipping.country });
        this.setState({ state: value.shipping.state });
        this.setState({ email: value.email });
        this.setState({ customer_id: value.id });

      }
    } catch (error) {
      console.log(error);
    }

    var response = await services.restGetHandle('shipping/zones');
    response.forEach((element, i) => {
      if(element.name == "Locations not covered by your other zones"){
        response.splice(i, 1);
      }
    });
    if(response.length == 1){
      this.setState({country: response[0].name})
      this.setState({country_id: response[0].id})
    }
    this.setState({country_array : response});
  };

  validate = async () => {
    if (
      this.state.first_name ||
      this.state.last_name ||
      this.state.state ||
      this.state.city ||
      this.state.zip ||
      this.state.country ||
      this.state.address
    ) {
      return true;
    } else {
      Toast.show({
        text: "Fill all fields to proceed",
        buttonText: "Okay",
      });
      return false;
    }
  };

  shipping = async () => {
    // var validate = await this.validate();
    // if (validate) {
    //   var self = this;
    //   self.setState({ addingToCart: true });
    //   try {
    //     cart = await AsyncStorage.getItem("cart");
    //     cart = await JSON.parse(cart);
    //     console.log(cart);
    //   } catch (error) {
    //     console.log(error);
    //   }
    //   line_items = [];
    //   cart.forEach((element) => {
    //     var obj = {
    //       product_id: element.product_id,
    //       quantity: element.quantity,
    //       variation_id : element.variation_id,
    //     };
    //     line_items.push(obj);
    //   });
    //   var data = {
    //     payment_method: "cod",
    //     payment_method_title: "Cash on delivery",
    //     set_paid: true,
    //     status: 'processing',
    //     customer_id: this.state.customer_id,
    //     billing: {
    //       first_name: this.state.first_name,
    //       last_name: this.state.state.last_name,
    //       address_1: this.state.address,
    //       address_2: this.state.address2,
    //       city: this.state.city,
    //       state: this.state.state,
    //       postcode: this.state.zip,
    //       country: this.state.country,
    //       email: this.state.email,
    //     },
    //     shipping: {
    //       first_name: this.state.first_name,
    //       last_name: this.state.state.last_name,
    //       address_1: this.state.address,
    //       address_2: this.state.address2,
    //       city: this.state.city,
    //       state: this.state.state,
    //       postcode: this.state.zip,
    //       country: this.state.country,
    //     },
    //     line_items: line_items,
    //     shipping_lines: [
    //       {
    //         method_id: "flat_rate",
    //         method_title: "Flat Rate",
    //         total: "0.00",
    //       },
    //     ],
    //   };

    //   await AsyncStorage.setItem("shipping_data", JSON.stringify(data));
    //   this.props.navigation.goBack();

    //   self.setState({ addingToCart: false });
    // }
    this.setState({addingToCart: true});
    var country_location = await services.restGetHandle('shipping/zones/'+this.state.country_id+'/locations');
    var country_code = country_location[0].code;
    var updateCustomerData = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      billing: {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        address_1: this.state.address,
        address_2: this.state.address2,
        city: this.state.city,
        country: country_code
      },
      shipping: {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        address_1: this.state.address,
        address_2: this.state.address2,
        city: this.state.city,
        country: country_code
      }
    }
    var response = await services.restPostHandle('customers/'+this.props.userData.id.toString(), updateCustomerData);
    console.log(response);
    await AsyncStorage.setItem("userData", JSON.stringify(response));
    await this.props.dispatch(userData(response));
    this.setState({addingToCart:false});
    Toast.show({
      text: "Address updated",
      buttonText: "Okay",
      duration: 3000,
    });
    await this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header navigation={this.props.navigation} goBack={true}/>
        <View style={{ flexDirection: "row", padding: 12 }}>
          <TouchableOpacity
            style={{ flex: 1, marginTop: 12, alignItems: "center" }}
            onPress={() => {
              this.setState({ selected: "shipping" });
            }}
          >
            <View
              style={{
                paddingBottom: 24,
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                borderBottomWidth: 1,
                borderBottomColor:
                  this.state.selected == "shipping" ? "#000" : "#ccc",
              }}
            >
              <Text
                style={{
                  fontSize: 22,
                  fontWeight: "bold",
                  color: this.state.selected == "shipping" ? "#000" : "#ccc",
                  marginTop: 12,
                  marginBottom: 12,
                }}
              >
                Shipping & Billing
              </Text>
            </View>
            {this.state.selected == "shipping" ? (
              <AntDesign
                name="caretdown"
                size={24}
                color="black"
                style={{ marginTop: -8 }}
              />
            ) : null}
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={{ width: "100%", padding: 24 }}>
            {/* {this.state.hasSavedAddress ? (
              <ListItem>
                <Left>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ useSaved: !this.state.useSaved }, () => {
                        if (this.state.useSaved) {
                          this.setState({
                            first_name: this.state.addressData.first_name,
                          });
                          this.setState({
                            last_name: this.state.addressData.last_name,
                          });
                          this.setState({ city: this.state.addressData.city });
                          this.setState({
                            address: this.state.addressData.address,
                          });
                          this.setState({
                            country: this.state.addressData.country,
                          });
                        } else {
                          this.setState({ first_name: "" });
                          this.setState({ last_name: "" });
                          this.setState({ city: "" });
                          this.setState({ address: "" });
                          this.setState({ country: "" });
                        }
                      });
                    }}
                  >
                    <Text>{this.props.appContent.use_saved_address}</Text>
                  </TouchableOpacity>
                </Left>
                <Right>
                  <Radio
                    selected={this.state.useSaved ? true : false}
                    onPress={() => {
                      this.setState({ useSaved: !this.state.useSaved }, () => {
                        if (this.state.useSaved) {
                          this.setState({
                            first_name: this.state.addressData.first_name,
                          });
                          this.setState({
                            last_name: this.state.addressData.last_name,
                          });
                          this.setState({ city: this.state.addressData.city });
                          this.setState({
                            address: this.state.addressData.address,
                          });
                          this.setState({
                            country: this.state.addressData.country,
                          });
                        } else {
                          this.setState({ first_name: "" });
                          this.setState({ last_name: "" });
                          this.setState({ city: "" });
                          this.setState({ address: "" });
                          this.setState({ country: "" });
                        }
                      });
                    }}
                  />
                </Right>
              </ListItem>
            ) : null} */}

            {/* {this.state.hasSavedAddress ? (
              <ListItem>
                <View>
                <Text>{this.state.addressData.first_name}</Text>
                <Text>{this.state.addressData.last_name},</Text>
                <Text>{this.state.addressData.address},</Text>
                <Text>{this.state.addressData.city},</Text>
                <Text>{this.state.addressData.country}.</Text>
                </View>
                
              </ListItem>
            ) : null} */}

            <Text style={{ marginTop: 12 }}>
              {this.props.appContent.add_your_address}
            </Text>

            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
              {this.props.appContent.first_name}
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(first_name) => this.setState({ first_name })}
              value={this.state.first_name}
            />

            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
              {this.props.appContent.last_name}
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(last_name) => this.setState({ last_name })}
              value={this.state.last_name}
            />

            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
              {this.props.appContent.address} 1
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(address) => this.setState({ address })}
              value={this.state.address}
            />

            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
              {this.props.appContent.address} 2
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(address2) => this.setState({ address2 })}
              value={this.state.address2}
            />

            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
              {this.props.appContent.city}
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(city) => this.setState({ city })}
              value={this.state.city}
            />

            {/* <Text style={{ marginTop: 24, fontWeight: "bold" }}>STATE*</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(state) => this.setState({ state })}
          value={this.state.state}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>ZIP*</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(zip) => this.setState({ zip })}
          value={this.state.zip}
        /> */}

            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
              {this.props.appContent.country}
            </Text>
            <Picker
              note
              mode="dropdown"
              iosHeader="Select your country"
              iosIcon={
                <Icon
                  name="arrow-down"
                  style={{ color: "#ccc", marginLeft: -12 }}
                />
              }
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              placeholder="Your Country"
              selectedValue={this.state.country_id}
              onValueChange={(value) => {
                var countryy= this.state.country_array.filter(i=> i.id == value);
                this.setState({ country: countryy[0].name });
                this.setState({ country_id: value });
              }}
            >
              {this.state.country_array.map((item) => (
                <Picker.Item label={item.name} value={item.id} />
              ))}
            </Picker>

            <TouchableOpacity
              style={{ marginTop: 24, marginBottom: 24 }}
              onPress={this.shipping}
            >
              <View
                style={{
                  width: "100%",
                  backgroundColor: "#000000",
                  alignItems: "center",
                  justifyContent: "center",
                  padding: 12,
                }}
              >
                {this.state.addingToCart ? (
                  <Spinner color="#fff" style={{ color: "#fff" }} />
                ) : (
                  <Text
                    style={{
                      color: "#fff",
                      fontWeight: "bold",
                      fontSize: 18,
                    }}
                  >
                    {this.props.appContent.proceed}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          </View>

          <Footer navigation={this.props.navigation} />
        </ScrollView>
      </View>
    );
  }
}

Checkout.navigationOptions = {
  header: null,
};

import { connect } from "react-redux";
import services from "../services";
const mapStateToProps = (state) => {
  const appContent = state.appContent;
  const appContentType = state.appContentType;
  const userData = state.userData;

  return { appContent, appContentType, userData };
};

export default connect(mapStateToProps)(Checkout);
