const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
const CART_COUNT = 'CART_COUNT';
const USER_DATA = 'USER_DATA';
const APP_CONTENT = 'APP_CONTENT';
const APP_CONTENT_TYPE = 'APP_CONTENT_TYPE';

const increment = () => ({type: INCREMENT_COUNTER});
const cartCount = data => ({type: CART_COUNT, data});
const userData = data => ({type: USER_DATA, data});
const appContent = data => ({type: APP_CONTENT, data});
const appContentType = data => ({type: APP_CONTENT_TYPE, data});

export {
  INCREMENT_COUNTER,
  CART_COUNT,
  USER_DATA,
  APP_CONTENT,
  APP_CONTENT_TYPE,
  increment,
  cartCount,
  userData,
  appContent,
  appContentType,
};
