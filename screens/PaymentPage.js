import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  Dimensions,
  ScrollView,
  StyleSheet,
  AsyncStorage,
  Modal,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { Spinner, Toast } from "native-base";
import services from "../services";
import {WebView} from "react-native-webview";
import {
  EvilIcons,
  AntDesign,
  Entypo,
  Feather,
  SimpleLineIcons,
} from "@expo/vector-icons";
class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      link: "",
    };
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", () => {
      console.log(this.props.route.params.link);
      this.setState({ link: this.props.route.params.link });
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            backgroundColor: "#fff",
            flexDirection: "row",
            alignItems: "center",
            height: 80,
            paddingTop: Platform.OS === "android" ? 16 : 0,
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              height: "100%",
              marginLeft: 12,
            }}
          >
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <AntDesign
                name="arrowleft"
                style={{ fontSize: 30, color: "#000000" }}
              />
            </TouchableOpacity>
          </View>
        </View>
        {this.props.route.params.link ? 
        <WebView
          source={{ uri: this.props.route.params.link}}
          style={{ flex: 1 }}
        /> : null}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const userData = state.userData;
  const appContent = state.appContent;
  const appContentType = state.appContentType;

  return { appContent, appContentType, userData };
};

export default connect(mapStateToProps)(Cart);
