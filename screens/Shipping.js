import React, { Component } from "react";
import { View, Image, TextInput, Text, AsyncStorage } from "react-native";
import { CommonActions } from "@react-navigation/native";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Picker, Icon, Toast, Spinner } from "native-base";
import axios from "axios";

class Shipping extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      address: "",
      city: "",
      state: "",
      zip: "",
      country: "",
      userData: {},
      orderDetails: {},
      addingToCart: false,
    };
  }

  async componentDidMount() {
    this.getUser();
    this.getOrderDetails();
  }

  getUser = async () => {
    try {
      const value = await AsyncStorage.getItem("userData");
      console.log(value);
      if (value !== null) {
        this.setState({ userData: JSON.parse(value) });
      }
    } catch (error) {
      console.log(error);
    }
  };

  getOrderDetails = async () => {
    try {
      const value = await AsyncStorage.getItem("orderDetails");
      console.log(value);
      if (value !== null) {
        this.setState({ orderDetails: JSON.parse(value) });
      }
    } catch (error) {
      console.log(error);
    }
  };

  validate = async () => {
    if (
      this.state.first_name ||
      this.state.last_name ||
      this.state.state ||
      this.state.city ||
      this.state.zip ||
      this.state.country
    ) {
      return true;
    } else {
      Toast.show({
        text: "Fill all fields to proceed",
        buttonText: "Okay",
      });
      return false;
    }
  };

  shipping = async () => {
    var validate = await this.validate();
    if (validate) {
      var self = this;
      self.setState({addingToCart: true});
      var post = {
        customer_id: self.state.userData.id,
        customer_email: self.state.userData.email,
        customer_first_name: self.state.userData.first_name,
        customer_last_name: self.state.userData.last_name,
        billing_first_name: self.state.first_name,
        billing_last_name: self.state.last_name,
        billing_address_1: self.state.address,
        billing_city: self.state.city,
        billing_state: self.state.state,
        billing_zip: self.state.zip,
        billing_country: self.state.country,
        shipping_first_name: self.state.first_name,
        shipping_last_name: self.state.last_name,
        shipping_address_1: self.state.address,
        shipping_city: self.state.city,
        shipping_state: self.state.state,
        shipping_zip: self.state.zip,
        shipping_country: self.state.country,
        sub_total: self.state.orderDetails.sub_total,
        shipping_method: "Cash on Delivery",
        shipping_cost: 0,
        discount: 0,
        total: self.state.orderDetails.sub_total,
        payment_method: "Cash on Delivery",
        currency: self.state.orderDetails.currency,
        currency_rate: self.state.orderDetails.currency_rate,
        locale: self.state.orderDetails.locale,
        status: self.state.orderDetails.status,
      };

      await AsyncStorage.setItem("shipping_data", JSON.stringify(post));
      this.props.navigation.navigate('Payment');

      // axios
      //   .post("https://phone7m.shop/sys/public/api/cart/checkout", post)
      //   .then(async function (response) {
      //     console.log(response.data);
      //     if (response.data.success) {
      //       Toast.show({
      //         text: "Your order has been placed",
      //         buttonText: "Okay",
      //         duration: 3000,
      //       });
            
      //       self.props.navigation.dispatch(
      //         CommonActions.reset({
      //           index: 0,
      //           routes: [
      //             {
      //               name: "Home",
      //             },
      //           ],
      //         })
      //       );
      //     } else {
      //       Toast.show({
      //         text: "Something went wrong",
      //         buttonText: "Okay",
      //         duration: 3000,
      //       });
      //     }
      //     self.setState({addingToCart: false});
      //   })
      //   .catch(function (error) {
      //     console.log(error);
      //     Toast.show({
      //       text: "Something went wrong",
      //       buttonText: "Okay",
      //       duration: 3000,
      //     });
      //     self.setState({addingToCart: false});
      //   });
    }
  };

  render() {
    return (
      <View style={{ width: "100%", padding: 24 }}>
        <Text style={{ marginTop: 12 }}>{this.props.appContent.add_your_address}</Text>

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>{this.props.appContent.first_name}</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(first_name) => this.setState({ first_name })}
          value={this.state.first_name}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>{this.props.appContent.last_name}</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(last_name) => this.setState({ last_name })}
          value={this.state.last_name}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>{this.props.appContent.address}</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(address) => this.setState({ address })}
          value={this.state.address}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>{this.props.appContent.city}</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(city) => this.setState({ city })}
          value={this.state.city}
        />

        {/* <Text style={{ marginTop: 24, fontWeight: "bold" }}>STATE*</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(state) => this.setState({ state })}
          value={this.state.state}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>ZIP*</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(zip) => this.setState({ zip })}
          value={this.state.zip}
        /> */}

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>{this.props.appContent.country}</Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          onChangeText={(country) => this.setState({ country })}
          value={this.state.country}
        />

        <TouchableOpacity
          style={{ marginTop: 24, marginBottom: 24 }}
          onPress={this.shipping}
        >
          <View
            style={{
              width: "100%",
              backgroundColor: "#000000",
              alignItems: "center",
              justifyContent: "center",
              padding: 12,
            }}
          >
            <Text style={{ color: "#fff", fontWeight: "bold" }}>
              {this.state.addingToCart ? (
                <Spinner color="#fff" style={{ color: "#fff" }} />
              ) : (
                <Text
                  style={{
                    color: "#fff",
                    fontWeight: "bold",
                    fontSize: 18,
                  }}
                >
                  {this.props.appContent.proceed}
                </Text>
              )}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
   
   );
  }
}

import {connect} from 'react-redux'; 
const mapStateToProps = (state) => {
 
 const appContent = state.appContent;
const appContentType = state.appContentType;

  return { appContent , appContentType};
};

export default connect(mapStateToProps)(Shipping);


