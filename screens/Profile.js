import React, { Component } from "react";
import { View, Image, TextInput, Text, AsyncStorage } from "react-native";

import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import { Picker, Icon, Toast } from "native-base";
import axios from "axios";
import Header from "./Header";
import { CommonActions } from "@react-navigation/native";
import country_codes from "../constants/country_codes.json";
import services from "../services"

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      first_name: "",
      last_name: "",
      password: "",
      mobile: "",
      confirm_password: "",
      country_initals: "",
      userData: {},
    };
  }

  async componentDidMount() {
    console.log(country_codes);
    this.getOldUser();
  }

  getOldUser = async () => {
    try {
      const value = await AsyncStorage.getItem("userData");
      console.log(value);
      if (value !== null) {
        this.setState({ userData: JSON.parse(value) }, this.getuser);
      }
    } catch (error) {
      console.log(error);
    }
  };

  getuser = async () => {
    var response = await services.restGetHandle('customers/'+this.state.userData.id);
    this.setState({ email: response.email });
    this.setState({ first_name: response.first_name });
    this.setState({ last_name: response.last_name });
    this.setState({userData: response});
    await AsyncStorage.setItem(
      "userData",
      JSON.stringify(response)
    );
  };

  register = async () => {
    var post = {};
    post.email = this.state.email;
    post.first_name = this.state.first_name;
    post.last_name = this.state.last_name;
    var response = await services.restPostHandle('customers/'+this.state.userData.id, post);
    await AsyncStorage.setItem(
      "userData",
      JSON.stringify(response)
    );
    Toast.show({
      text: "Profile updated successfully",
      buttonText: "Okay",
      duration: 3000,
    });
  };

  render() {
    return (
      <View style={{ width: "100%"}}>
        {/* <Text style={{ marginTop: 12 }}>
          Join now and earn up tp 35,000 loyality dots
        </Text> */}

         <Header navigation={this.props.navigation} goBack={true}/>
        <ScrollView >
          <View style={{flex: 1, padding: 12}}>
            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
            {this.props.appContent.email}
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(email) => this.setState({ email })}
              keyboardType="email-address"
              placeholder="abc@exapmple.com"
              value={this.state.email}
            />

            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
            {this.props.appContent.first_name}
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(first_name) => this.setState({ first_name })}
              value={this.state.first_name}
            />

            <Text style={{ marginTop: 24, fontWeight: "bold" }}>
            {this.props.appContent.last_name}
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(last_name) => this.setState({ last_name })}
              value={this.state.last_name}
            />

            {/* <Text style={{ marginTop: 24, fontWeight: "bold" }}>{this.props.appContent.mobile}</Text>
            <View style={{ flexDirection: "row", marginTop: 12 }}>
              <TextInput
                style={{
                  borderWidth: 1,
                  borderColor: "#ccc",
                  backgroundColor: "#fff",

                  padding: 12,
                  flex: 1,
                }}
                keyboardType="number-pad"
                onChangeText={(mobile) => this.setState({ mobile })}
                value={this.state.mobile}
              />
            </View> */}

            {/* <Text style={{ marginTop: 24, fontWeight: "bold" }}>
            {this.props.appContent.create_password}
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              secureTextEntry={true}
              onChangeText={(password) => this.setState({ password })}
              value={this.state.password}
            />

            <Text style={{ color: "#ccc", marginTop: 12 }}>
              Minimum length is 6 characters
            </Text> */}

            {/* <Text style={{ marginTop: 24, fontWeight: "bold" }}>
            {this.props.appContent.confirm_password}
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#ccc",
                backgroundColor: "#fff",
                marginTop: 12,
                padding: 12,
              }}
              onChangeText={(confirm_password) =>
                this.setState({ confirm_password })
              }
              value={this.state.confirm_password}
              secureTextEntry={true}
            /> */}

            <TouchableOpacity
              style={{ marginTop: 24, marginBottom: 80 }}
              onPress={this.register}
            >
              <View
                style={{
                  width: "100%",
                  backgroundColor: "#000000",
                  alignItems: "center",
                  justifyContent: "center",
                  padding: 12,
                }}
              >
                <Text style={{ color: "#fff", fontWeight: "bold" }}>
                  {this.props.appContent.update}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

import {connect} from 'react-redux'; 
const mapStateToProps = (state) => {
 
 const appContent = state.appContent;
const appContentType = state.appContentType;

  return { appContent , appContentType};
};

export default connect(mapStateToProps)(Profile);


