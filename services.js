const BASE_URL = "https://hadoro.ae/wp-json/wc/v3/";
const BASE_URL_API = 'https://apis.hadoro.ae/api/';
import WooCommerceAPI from "react-native-woocommerce-api";
var param_secrets = "&consumer_key=ck_ecb8e55143d93d279dc599b6b2013b72054576db&consumer_secret=cs_0a6a79f9a8199c4bd5d06c15789ac7e186051c43"
var standard_secrets = "?consumer_key=ck_ecb8e55143d93d279dc599b6b2013b72054576db&consumer_secret=cs_0a6a79f9a8199c4bd5d06c15789ac7e186051c43"
export default {
    async restGetHandle(url) {
      console.log(BASE_URL, url);
      try {
        let response = await fetch(BASE_URL + url + standard_secrets, {
          method: 'GET',
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
        });
        return response.json();
      } catch (error) {
        console.log(error);
      }
    },
    async restGetHandleParams(url) {
        console.log(BASE_URL, url);
        try {
          let response = await fetch(BASE_URL + url + param_secrets, {
            method: 'GET',
            headers: new Headers({
              'Content-Type': 'application/json',
            }),
          });
          return response.json();
        } catch (error) {
          console.log(error);
        }
      },
    async restPostHandle(url, post) {
      console.log(BASE_URL, url);
      try {
        let response = await fetch(BASE_URL + url + standard_secrets, {
          method: 'POST',
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify(post),
        });
        return response.json();
      } catch (error) {
        console.log(error);
      }
    },
    async restGetHandleApi(url) {
      console.log(BASE_URL_API, url);
      try {
        let response = await fetch(BASE_URL_API + url, {
          method: 'GET',
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
        });
        return response.json();
      } catch (error) {
        console.log(error);
      }
    },
    async restPostHandleApi(url, post) {
      console.log(BASE_URL_API, url, post);
      try {
        let response = await fetch(BASE_URL_API + url, {
          method: 'POST',
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify(post),
        });
        return response.json();
      } catch (error) {
        console.log(error);
      }
    },
    handleCms(key, value) {
      store.dispatch(cmsKey(key));
      store.dispatch(cmsValue(value));
      store.dispatch(showCms());
    },
  };