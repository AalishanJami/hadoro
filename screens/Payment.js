import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  AsyncStorage,
  ScrollView,
} from "react-native";
import { ListItem, Radio, Right, Left, Toast, Spinner } from "native-base";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import axios from "axios";
import Header from "./Header";
import Footer from "./Footer";
import { CommonActions } from "@react-navigation/native";
import services from "../services";

class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: "",
      name: "",
      cvv: "",
      exp: "",
      howPay: true,
      post: {},
      addingToCart: false,
    };
  }

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem("shipping_data");
      if (value !== null) {
        console.log(JSON.parse(value));
        console.log("json parse object");
        this.setState({ post: JSON.parse(value) });
      }
    } catch (error) {
      console.log(error);
    }
  }

  shipping = async () => {
    var self = this;
    self.setState({ addingToCart: true });
    var post = this.state.post;
    console.log(post)
    var create_response = await services.restPostHandle("orders", post);
    console.log(create_response);

    Toast.show({
      text: "Your order has been placed",
      buttonText: "Okay",
      duration: 3000,
    });
    await AsyncStorage.removeItem("cart");
    await AsyncStorage.removeItem("orderDetails");
    self.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: "Home",
          },
        ],
      })
    );
    
    self.setState({ addingToCart: false });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
         <Header navigation={this.props.navigation} goBack={true}/>
        <View style={{ flexDirection: "row", padding: 12 }}>
          <TouchableOpacity
            style={{ flex: 1, marginTop: 12, alignItems: "center" }}
            onPress={() => {
              this.setState({ selected: "payment" });
            }}
          >
            <View
              style={{
                paddingBottom: 24,
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                borderBottomWidth: 1,
                borderBottomColor:
                  this.state.selected == "payment" ? "#000" : "#ccc",
              }}
            >
              <Text
                style={{
                  fontSize: 22,
                  fontWeight: "bold",
                  color: this.state.selected == "payment" ? "#000" : "#ccc",
                  marginTop: 12,
                  marginBottom: 12,
                }}
              >
                Payment
              </Text>
            </View>
            {this.state.selected == "payment" ? (
              <AntDesign
                name="caretdown"
                size={24}
                color="black"
                style={{ marginTop: -8 }}
              />
            ) : null}
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={{ width: "100%", padding: 24 }}>
            <ListItem>
              <Left>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ howPay: !this.state.howPay });
                  }}
                >
                  <Text>{this.props.appContent.cash_on_delivery}</Text>
                </TouchableOpacity>
              </Left>
              <Right>
                <Radio
                  selected={this.state.howPay ? true : false}
                  onPress={() => {
                    this.setState({ howPay: !this.state.howPay });
                  }}
                />
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ howPay: !this.state.howPay });
                  }}
                >
                  <Text>{this.props.appContent.credit_debit}</Text>
                </TouchableOpacity>
              </Left>
              <Right>
                <Radio
                  selected={this.state.howPay ? false : true}
                  onPress={() => {
                    this.setState({ howPay: !this.state.howPay });
                  }}
                />
              </Right>
            </ListItem>
            {this.state.howPay ? null : (
              <View>
                <Text style={{ marginTop: 24, fontWeight: "bold" }}>
                  CARD HOLDER NAME*
                </Text>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: "#ccc",
                    backgroundColor: "#fff",
                    marginTop: 12,
                    padding: 12,
                  }}
                  value={this.state.name}
                  onChangeText={(name) => this.setState({ name })}
                />
                <Text style={{ marginTop: 24, fontWeight: "bold" }}>
                  CARD NUMBER*
                </Text>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: "#ccc",
                    backgroundColor: "#fff",
                    marginTop: 12,
                    padding: 12,
                  }}
                  value={this.state.card}
                  onChangeText={(card) => this.setState({ card })}
                />
                <View style={{ flexDirection: "row" }}>
                  <View style={{ flex: 1, marginRight: 12 }}>
                    <Text style={{ marginTop: 24, fontWeight: "bold" }}>
                      CVV*
                    </Text>
                    <TextInput
                      style={{
                        borderWidth: 1,
                        borderColor: "#ccc",
                        backgroundColor: "#fff",
                        marginTop: 12,
                        padding: 12,
                      }}
                      value={this.state.cvv}
                      secureTextEntry={true}
                      onChangeText={(cvv) => this.setState({ cvv })}
                    />
                  </View>
                  <View style={{ flex: 1, marginLeft: 12 }}>
                    <Text style={{ marginTop: 24, fontWeight: "bold" }}>
                      EXP DD/MM*
                    </Text>
                    <TextInput
                      style={{
                        borderWidth: 1,
                        borderColor: "#ccc",
                        backgroundColor: "#fff",
                        marginTop: 12,
                        padding: 12,
                      }}
                      value={this.state.exp}
                      secureTextEntry={true}
                      onChangeText={(exp) => this.setState({ exp })}
                    />
                  </View>
                </View>
              </View>
            )}

            <TouchableOpacity
              style={{ marginTop: 24, marginBottom: 24 }}
              onPress={this.shipping}
            >
              <View
                style={{
                  width: "100%",
                  backgroundColor: "#000000",
                  alignItems: "center",
                  justifyContent: "center",
                  padding: 12,
                }}
              >
                {this.state.addingToCart ? (
                  <Spinner color="#fff" style={{ color: "#fff" }} />
                ) : (
                  <Text style={{ color: "#fff", fontWeight: "bold" }}>
                    {this.props.appContent.checkout}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
         
          </View>
          <Footer navigation={this.props.navigation} />
        </ScrollView>
      </View>
    );
  }
}

import {connect} from 'react-redux'; 
const mapStateToProps = (state) => {
 
 const appContent = state.appContent;
const appContentType = state.appContentType;

  return { appContent , appContentType};
};

export default connect(mapStateToProps)(Payment);


