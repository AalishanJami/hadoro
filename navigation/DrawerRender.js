import React, { Component } from "react";
import { View, Image, TextInput, Text } from "react-native";
import {
  EvilIcons,
  AntDesign,
  Entypo,
  Feather,
  Ionicons,
} from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Spinner } from "native-base";
import axios from "axios";
import { connect } from "react-redux";
import { CommonActions } from "@react-navigation/native";
import services from "../services";

class DrawerRender extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      parent: [],
      current_category: [],
      index_of_category: null,
      show_partial: true,
    };
  }

  async componentDidMount() {}

  getAll = async (id) => {
    var self = this;
    var response = await services.restGetHandleParams(
      "products/categories?parent=" + id
    );
    response.forEach((element, i) => {
      if (
        element.name == "Featured Products" ||
        element.name == "Top Categories" ||
        element.name == "Our Selection"
      ) {
        response.splice(i, 1);
      }
    });
    self.setState({ categories: response, show_partial: false });
  };

  render() {
    categories = this.state.categories;

    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            backgroundColor: "#fff",
            flexDirection: "row",
            alignItems: "center",
            height: 100,
            paddingTop: Platform.OS === "android" ? 16 : 0,
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              height: "100%",
              marginLeft: 12,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                console.log(this.props.navigation.closeDrawer());
                this.props.navigation.closeDrawer();
              }}
            >
              <EvilIcons
                name="close"
                style={{ fontSize: 30, color: "#000000" }}
              />
            </TouchableOpacity>
            
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Home")} style={{alignItems:"center", marginLeft: 12}}
            >
              <Image
                source={require("../assets/images/hadoro_main_icon.png")}
                style={{ height: 100, width: 100 }}
                resizeMode="contain"
              />
            </TouchableOpacity>

           
          </View>
        </View>
        <View
          style={{
            height: 50,
            backgroundColor: "#80808020",
            justifyContent: "center",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text style={{ marginLeft: 12, fontWeight: "bold", flex: 1 }}>
            Categories
          </Text>
          {this.state.show_partial ? null : (
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  show_partial: true, categories: []
                })
              }}
            >
              <AntDesign
                name="arrowleft"
                style={{ fontSize: 18, color: "#000000", marginRight: 12 }}
              />
            </TouchableOpacity>
          )}
        </View>
        {this.state.show_partial ? (
          <View>
            <View
              style={{
                height: 50,
                borderBottomColor: "#80808040",
                borderBottomWidth: 1,
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, justifyContent: "center" }}
                onPress={() => {
                  this.getAll(110);
                }}
              >
                <Text style={{ marginLeft: 12, fontWeight: "bold" }}>
                  Cases
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: 50,
                borderBottomColor: "#80808040",
                borderBottomWidth: 1,
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, justifyContent: "center" }}
                onPress={() => {
                  this.getAll(106);
                }}
              >
                <Text style={{ marginLeft: 12, fontWeight: "bold" }}>Grip</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : null}

        {categories.map((item) => (
          <View
            style={{
              height: 50,
              borderBottomColor: "#80808040",
              borderBottomWidth: 1,
              alignItems: "center",
              flexDirection: "row",
            }}
            key={item.id.toString()}
          >
            <TouchableOpacity
              style={{ flex: 1, justifyContent: "center" }}
              onPress={() => {
                this.props.navigation.navigate("GetProducts", {
                  id: item.id,
                  type: "category",
                  name: item.name,
                });
                this.props.navigation.closeDrawer();
              }}
            >
              <Text style={{ marginLeft: 12, fontWeight: "bold" }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          </View>
        ))}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const appContent = state.appContent;
  const appContentType = state.appContentType;

  return { appContent, appContentType };
};

export default connect(mapStateToProps)(DrawerRender);
