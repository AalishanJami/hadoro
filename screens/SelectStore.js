import React, { Component } from "react";
import { View, Image, TextInput, Text } from "react-native";
import Footer from "./Footer";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import Header from "./Header";
import { Spinner } from "native-base";
import axios from "axios";
import { CommonActions } from "@react-navigation/native";

class SelectStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      brands: [],
      isLoading: true,
    };
  }

  async componentDidMount() {
    this.getStores();
  }

  getStores = () => {
    var self = this;
    axios
      .get("https://phone7m.shop/sys/public/api/brand")
      .then(function (response) {
        self.setState({ brands: response.data.data.brands });
        console.log(response.data.data);
        self.setState({ isLoading: false });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
         <Header navigation={this.props.navigation} goBack={true}/>
        <View>
          <View
            style={{
              height: 50,
              backgroundColor: "#e0e0e0",
              justifyContent: "center",
            }}
          >
            <Text style={{ marginLeft: 12, fontWeight: "bold" }}>
              SELECT A BRAND
            </Text>
          </View>
          {this.state.isLoading ? (
            <Spinner style={{ color: "#000000" }} />
          ) : null}
          {this.state.brands.map((item) => (
            <View
              style={{
                padding: 12,
                borderColor: "#ccc",
                borderBottomWidth: 1,
                marginLeft: 12,
                marginRight: 12,
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.dispatch(
                    CommonActions.reset({
                      index: 1,
                      routes: [
                        {
                          name: "GetProducts",
                          params: {
                            id: item.id,
                            type: "brands",
                            name: item.name,
                          },
                        },
                      ],
                    })
                  );
                }}
              >
                <Text style={{ fontSize: 22 }}>{item.name}</Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>
      </View>
    );
  }
}

import {connect} from 'react-redux'; 
const mapStateToProps = (state) => {
 
 const appContent = state.appContent;
const appContentType = state.appContentType;

  return { appContent , appContentType};
};

export default connect(mapStateToProps)(SelectStore);


