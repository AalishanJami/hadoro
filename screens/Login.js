import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  AsyncStorage,
  Alert,
  Dimensions,
} from "react-native";
import { Picker, Icon, Toast, Spinner } from "native-base";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import axios from "axios";
import { CommonActions } from "@react-navigation/native";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";
import { userData, cartCount } from "../redux-store/action";
import { connect } from "react-redux";
import * as AppleAuthentication from "expo-apple-authentication";
import services from "../services";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      isLoading: false,
      errortext: "",
    };
  }

  async componentDidMount() {}

  validate = async () => {
    if (this.state.email.length > 0 && this.state.password.length > 0) {
      return true;
    } else {
      Toast.show({
        text: "Fill all fields to contine",
        buttonText: "Okay",
      });
      return false;
    }
  };

  login = async () => {
    var validate = await this.validate();
    if (validate) {
      this.setState({ isLoading: true });
      var self = this;
      axios
        .post("https://hadoro.ae/wp-json/myplugin/v1/mobile-login", {
          email: this.state.email,
          password: this.state.password,
        })
        .then(async response => {
          console.log(response.data);
          if(response.data){
            var response_s = await services.restGetHandleParams(
              "customers?email=" + this.state.email
            );
            if (response_s.length > 0) {
              await AsyncStorage.setItem("userData", JSON.stringify(response_s[0]));
              this.props.dispatch(userData(response_s[0]));
              Toast.show({
                text: "Hi, " + response_s[0].first_name,
                buttonText: "Okay",
                duration: 3000,
              });
              this.props.navigation.navigate("Home");
            }
          } else {
            Toast.show({
              text: "Invalid Credentials",
              buttonText: "Okay",
              duration: 3000,
            });
          }
          this.setState({isLoading: false})
        })
        .catch(function (error) {
          console.log(error);
          self.setState({ isLoading: false });
        });
    }
  };

  logInwithFb = async () => {
    this.setState({ isLoading: true });
    try {
      await Facebook.initializeAsync({
        appId: "463229525048905",
      }).catch((error) => {
        alert(`Facebook initializeAsync: ${error}`);
      });
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ["public_profile", "email"],
        behavior: "web",
      }).catch((error) => {
        alert(`Facebook logInWithReadPermissionsAsync: ${error}`);
      });

      if (type === "success") {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(
          `https://graph.facebook.com/me?fields=id,name,picture.type(large),email&access_token=${token}`
        );
        //Alert.alert('Logged in!', `Hi ${(await response.json())}!`);
        //console.log(await response.json());
        var fbresponse = await response.json();
        var self = this;
        console.log(fbresponse);
        console.log(
          fbresponse.email,
          fbresponse.name.split(" ")[0],
          fbresponse.name.split(" ")[1]
        );
        var response_s = await services.restGetHandleParams(
          "customers?email=" + fbresponse.email
        );
        if (response_s.length > 0) {
          await AsyncStorage.setItem("userData", JSON.stringify(response_s[0]));
          this.props.dispatch(userData(response_s[0]));
          Toast.show({
            text: "Hi, " + response_s[0].first_name,
            buttonText: "Okay",
            duration: 3000,
          });
          this.props.navigation.navigate("Home");
        } else {
          var create_response = await services.restPostHandle("customers", {
            email: fbresponse.email,
            first_name: fbresponse.name.split(" ")[0],
            last_name: fbresponse.name.split(" ")[0],
            username: fbresponse.email,
          });
          await AsyncStorage.setItem(
            "userData",
            JSON.stringify(create_response)
          );
          this.props.dispatch(userData(create_response));
          Toast.show({
            text: "Hi, " + create_response.first_name,
            buttonText: "Okay",
            duration: 3000,
          });
          this.props.navigation.navigate("Home");
        }
      } else {
        // type === 'cancel'
        this.setState({ isLoading: false });
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
      console.log(message);
      this.setState({ isLoading: false });
    }
  };

  loginWithGoogle = async () => {
    const config = {
      iosClientId: `579584760768-k7blp5103skjpbqn27ffu8f8ikhos8c1.apps.googleusercontent.com`,
      androidClientId: `579584760768-i4opu80imgnvr5i141oe133bvai4qgck.apps.googleusercontent.com`,
      androidStandaloneAppClientId: `579584760768-n1hk9qbu9lg3blio5kb1rqsh13uk7bcv.apps.googleusercontent.com`,
      iosStandaloneAppClientId: `579584760768-hda5ssbfbadfqj2h3crbpe7gpq9lhasr.apps.googleusercontent.com`,
    };
    this.setState({ isLoading: true });
    // First- obtain access token from Expo's Google API
    const { type, accessToken, user } = await Google.logInAsync(config);

    if (type === "success") {
      var response_s = await services.restGetHandleParams(
        "customers?email=" + user.email
      );
      if (response_s.length > 0) {
        await AsyncStorage.setItem("userData", JSON.stringify(response_s[0]));
        this.props.dispatch(userData(response_s[0]));
        Toast.show({
          text: "Hi, " + response_s[0].first_name,
          buttonText: "Okay",
          duration: 3000,
        });
        this.props.navigation.navigate("Home");
      } else {
        var create_response = await services.restPostHandle("customers", {
          email: user.email,
          first_name: user.givenName,
          last_name: user.familyName,
          username: user.email,
        });
        await AsyncStorage.setItem("userData", JSON.stringify(create_response));
        this.props.dispatch(userData(create_response));
        Toast.show({
          text: "Hi, " + create_response.first_name,
          buttonText: "Okay",
          duration: 3000,
        });
        this.props.navigation.navigate("Home");
      }
    } else {
      this.setState({ isLoading: false });
    }
  };

  loginWithApple = async (email, firstname, lastname) => {
    this.setState({ isLoading: true });
    var response_s = await services.restGetHandleParams(
      "customers?email=" + email
    );
    if (response_s.length > 0) {
      await AsyncStorage.setItem("userData", JSON.stringify(response_s[0]));
      this.props.dispatch(userData(response_s[0]));
      Toast.show({
        text: "Hi, " + response_s[0].first_name,
        buttonText: "Okay",
        duration: 3000,
      });
      this.props.navigation.navigate("Home");
    } else {
      var create_response = await services.restPostHandle("customers", {
        email: email,
        first_name: firstname,
        last_name: lastname,
        username: email,
      });
      await AsyncStorage.setItem("userData", JSON.stringify(create_response));
      this.props.dispatch(userData(create_response));
      Toast.show({
        text: "Hi, " + create_response.first_name,
        buttonText: "Okay",
        duration: 3000,
      });
      this.props.navigation.navigate("Home");
    }
  };

  render() {
    return (
      <View style={{ width: "100%", padding: 24 }}>
        <Text style={{ marginTop: 12 }}>
          {this.state.errortext
            ? this.state.errortext
            : this.props.appContent.login_benefits}
        </Text>
        
        <Text style={{ marginTop: 24, fontWeight: "bold" }}>
          {this.props.appContent.email}*
        </Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          value={this.state.email}
          onChangeText={(email) => this.setState({ email })}
        />

        <Text style={{ marginTop: 24, fontWeight: "bold" }}>
          {this.props.appContent.password}*
        </Text>
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#fff",
            marginTop: 12,
            padding: 12,
          }}
          value={this.state.password}
          secureTextEntry={true}
          onChangeText={(password) => this.setState({ password })}
        />

        <TouchableOpacity
          style={{ marginTop: 24, marginBottom: 24 }}
          onPress={this.login}
        >
          <View
            style={{
              width: "100%",
              backgroundColor: "#000000",
              alignItems: "center",
              justifyContent: "center",
              padding: 12,
            }}
          >
            {this.state.isLoading ? (
              <Spinner style={{ color: "#fff" }} color="#fff" />
            ) : (
              <Text style={{ color: "#fff", fontWeight: "bold" }}>
                {this.props.appContent.login}
              </Text>
            )}
          </View>
        </TouchableOpacity>
        <Text style={{alignSelf:"center", marginTop: 12, marginBottom: 12, fontSize: 18, fontWeight: "bold"}}>OR</Text>
        <AppleAuthentication.AppleAuthenticationButton
          buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
          buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
          cornerRadius={5}
          style={{ width: Dimensions.get("window").width / 2, height: 40, alignSelf:"center" }}
          onPress={async () => {
            try {
              const credential = await AppleAuthentication.signInAsync({
                requestedScopes: [
                  AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
                  AppleAuthentication.AppleAuthenticationScope.EMAIL,
                ],
              });
              console.log(credential);
              if (credential.email) {
                await AsyncStorage.setItem("apple_email", credential.email);
                this.loginWithApple(
                  credential.email,
                  credential.fullName.givenName,
                  credential.fullName.familyName
                );
              } else {
                var email = await AsyncStorage.getItem("apple_email");
                this.loginWithApple(
                  email,
                  credential.fullName.givenName,
                  credential.fullName.familyName
                );
              }
              // signed in
            } catch (e) {
              if (e.code === "ERR_CANCELED") {
                Toast.show({
                  text: "Sign In Cancelled",
                  buttonText: "Okay",
                });
                // handle that the user canceled the sign-in flow
              } else {
                Toast.show({
                  text: "Error occured, please try again later",
                  buttonText: "Okay",
                });
                // this.setState({errortext: JSON.stringify(e)});
                // Alert.alert(JSON.stringify(e));
                // handle other errors
              }
            }
          }}
        />
        <TouchableOpacity onPress={this.loginWithGoogle} style={{marginTop: 6, alignSelf:"center"}}>
          <View
            style={{
              flexDirection: "row",
              height: 40,
              width: Dimensions.get("window").width / 2,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#fff",
              borderRadius: 5,
              borderColor: "#000",
              borderWidth: 1,
            }}
          >
            <Icon
              name="google"
              type="AntDesign"
              style={{ fontSize: 14, color: "#000" }}
            />
            <Text style={{ fontSize: 14, marginLeft: 4, color: "#000" }}>
              Sign in with Google
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.logInwithFb} style={{marginTop: 6, alignSelf:"center"}}>
          <View
            style={{
              flexDirection: "row",
              height: 40,
              width: Dimensions.get("window").width / 2,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#3b5998",
              borderRadius: 5,
            }}
          >
            <Icon
              name="facebook"
              type="Entypo"
              style={{ fontSize: 14, color: "#fff" }}
            />
            <Text style={{ fontSize: 14, marginLeft: 4, color: "#fff" }}>
              Sign in with Facebook
            </Text>
          </View>
        </TouchableOpacity>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const userData = state.userData;
  const appContent = state.appContent;
  const appContentType = state.appContentType;

  return { appContent, appContentType, userData };
};

export default connect(mapStateToProps)(Login);
