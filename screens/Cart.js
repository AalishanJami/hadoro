import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  Dimensions,
  ScrollView,
  StyleSheet,
  AsyncStorage,
  Modal,
} from "react-native";
import Footer from "./Footer";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import Header from "./Header";
import axios from "axios";
import { array, func } from "prop-types";
import { connect } from "react-redux";
import { Spinner, Toast } from "native-base";
import services from "../services";
import { CommonActions } from "@react-navigation/native";
class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      userData: {},
      isLoading: true,
      addingToCart: false,
      count: "",
      cart_id: "",
      isEmpty: true,
      grandTotal: 0,
      netTotal: 0,
      paymentModal: false,
      customer_sent_to_gateway: false,
      orderStatus: {},
      order_id: ""
    };
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener(
      "focus",
      async () => {
        console.log(this.state.customer_sent_to_gateway, "customer sent");
        if (this.state.customer_sent_to_gateway) {
          console.log("going inside customer sent")
          this.setState({ customer_sent_to_gateway: false });
          this.setState({ paymentModal: true });
          await this.processCardPayment();
        }
        this.getCart();
      }
    );
  }

  processCardPayment = async () => {
    console.log("in process card payment")
    this.setState({ paymentModal: false });
    if (await this.evaluatePayment()) {
      this.setState({ addingToCart: true });
      try {
        cart = this.state.products;
      } catch (error) {
        console.log(error);
        this.setState({ addingToCart: false });
      }
      var line_items = [];
      cart.forEach((element) => {
        var obj = {
          product_id: element.product_id,
          quantity: element.quantity,
          variation_id: element.variation_id,
        };
        line_items.push(obj);
      });
      var orderReference = await AsyncStorage.getItem("orderReference");
      var data = {
        payment_method: "ngenius",
        payment_method_title: "Pay By Card",
        set_paid: true,
        transaction_id: orderReference,
        status: "ng-complete",
        customer_id: this.props.userData.id,
        billing: this.props.userData.billing,
        shipping: this.props.userData.shipping,
        line_items: line_items,
        shipping_lines: [
          {
            method_id: "free_shipping",
            method_title: "Free shipping",
            total: "0.00",
          },
        ],
      };
      data.billing.email = this.props.userData.email;
      console.log(data);
      var create_response = await services.restPostHandle("orders", data);
      console.log(create_response);
      let id = create_response.id;
      this.setState({order_id : id}, this.updatePaymentDb);
      this.setState({ addingToCart: false });
      Toast.show({
        text: "Your order has been placed",
        buttonText: "Okay",
        duration: 3000,
      });
      await AsyncStorage.removeItem("cart");
      await AsyncStorage.removeItem("orderDetails");
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: "Home",
            },
          ],
        })
      );
    }
  };

  updatePaymentDb = async () => {
    let payment_id;
    let capture_id;
    console.log(this.state.orderStatus);
    let ids = this.state.orderStatus._embedded.payment[0]['_embedded']['cnp:capture'][0]['_links']['self']['href'];
    payment_id = ids.split('/').slice(-1)[0];
    capture_id = ids.split('/').slice(-3)[0];
    var response = await services.restPostHandleApi("updatePaymentDb", {
      order_id: this.state.order_id,
      "amount": parseInt(this.state.grandTotal)*100,
      "reference": this.state.orderStatus.reference,
      "payment_id" :payment_id,
      "capture_id": capture_id
    });
    console.log(response);
  }

  evaluatePayment = async() => {
    var token = await services.restGetHandleApi("getToken");
    console.log(token);
    await AsyncStorage.setItem("paymentToken", token);
    var orderReference = await AsyncStorage.getItem("orderReference");
    var response = await services.restPostHandleApi("saveOrder", {
      token: token,
      order_id: orderReference,
    });
    console.log(response);

    if(response?._embedded?.payment?.length>0){
      if(response?._embedded?.payment[0]?.authResponse?.success){
        this.setState({orderStatus: response});
        return true;
      }
    }
    return false;
  }

  getCart = async () => {
    this.setState({ isLoading: true });
    this.setState({ isEmpty: true });
    var cart = [];
    try {
      cart = await AsyncStorage.getItem("cart");
      if (cart == null) {
        cart = [];
        this.setState({ isLoading: false });
        this.setState({ isEmpty: true });
        this.setState({ grandTotal: 0 });
        this.setState({ netTotal: 0 });
      } else {
        cart = await JSON.parse(cart);
      }
      console.log(cart);
    } catch (error) {
      console.log(error);
      this.setState({ isLoading: false });
      this.setState({ isEmpty: true });
      this.setState({ grandTotal: 0 });
      this.setState({ netTotal: 0 });
    }
    if (cart?.length > 0) {
      this.setState({ products: cart });
      this.setState({ count: cart.length.toString() });
      this.setState({ isLoading: false });
      this.setState({ isEmpty: false });
      var total = 0;
      cart.forEach((element) => {
        var quantity = parseInt(element.quantity);
        if (quantity == 1) {
          total += parseFloat(element.cost);
        } else {
          total += parseFloat(element.cost) * quantity;
        }
      });
      this.setState({ netTotal: total });
      this.setState({ grandTotal: total });
    } else {
      this.setState({ isLoading: false });
      this.setState({ isEmpty: true });
      this.setState({ grandTotal: 0 });
      this.setState({ netTotal: 0 });
    }
    this.setState({ isLoading: false });
  };

  addToCart = async (product_id, quantity) => {
    this.setState({ addingToCart: true });
    var products = this.state.products;
    var index;
    products.forEach((element, i) => {
      if (element.product_id == product_id) {
        index = i;
      }
    });
    console.log(index);
    products[index]["quantity"] = quantity;
    this.setState({ cart: products });
    await AsyncStorage.setItem("cart", JSON.stringify(products));
    this.setState({ addingToCart: false });
    await this.getCart();
  };

  removeCart = async (product_id) => {
    this.setState({ addingToCart: true });
    var products = this.state.products;
    var index;
    products.forEach((element, i) => {
      if (element.product_id == product_id) {
        index = i;
      }
    });
    console.log(index);
    products.splice(index, 1);
    this.setState({ cart: products });
    await AsyncStorage.setItem("cart", JSON.stringify(products));
    this.setState({ addingToCart: false });
    await this.getCart();
  };

  checkout = async () => {
    if (this.state.userData != null) {
      this.proceed();
    } else {
      this.props.navigation.navigate("Auth");
      Toast.show({
        text: "Please login to proceed",
        buttonText: "Okay",
        duration: 3000,
      });
    }
  };
  checkoutCard = async () => {
    var validate = await this.validate();
    if (validate) {
      var link;
      var token = await services.restGetHandleApi("getToken");
      console.log(token);
      await AsyncStorage.setItem("paymentToken", token);
      this.setState({ paymentModal: true });
      if (!token) {
        Toast.show({
          text:
            "We are having problems connecting with the payment gateway, please try again later",
          buttonText: "Okay",
          duration: 2000,
        });
      } else {
        link = await services.restPostHandleApi("createOrder", {
          token: token,
          amount: this.state.grandTotal.toString(),
        });
        console.log(link);
        console.log(link._links.payment.href);
        await AsyncStorage.setItem("orderReference", link.reference);
        this.props.navigation.navigate("PaymentPage", {
          link: link._links.payment.href,
        });
        this.setState({ customer_sent_to_gateway: true, paymentModal: false });
      }
    }
  };

  validate = () => {
    if (
      this.props.userData.first_name &&
      this.props.userData.last_name &&
      this.props.userData.shipping.address_1 &&
      this.props.userData.shipping.address_2 &&
      this.props.userData.shipping.city &&
      this.props.userData.shipping.country
    ) {
      return true;
    } else {
      Toast.show({
        text: "Please complete your shipping address to proceed",
        buttonText: "Okay",
        duration: 2000,
      });
      return false;
    }
  };

  proceed = async () => {
    var validate = await this.validate();
    if (validate) {
      this.setState({ addingToCart: true });
      try {
        cart = this.state.products;
      } catch (error) {
        console.log(error);
        this.setState({ addingToCart: false });
      }
      var line_items = [];
      cart.forEach((element) => {
        var obj = {
          product_id: element.product_id,
          quantity: element.quantity,
          variation_id: element.variation_id,
        };
        line_items.push(obj);
      });
      var data = {
        payment_method: "cod",
        payment_method_title: "Cash on delivery",
        set_paid: true,
        status: "processing",
        customer_id: this.props.userData.id,
        billing: this.props.userData.billing,
        shipping: this.props.userData.shipping,
        line_items: line_items,
        shipping_lines: [
          {
            method_id: "free_shipping",
            method_title: "Free shipping",
            total: "0.00",
          },
        ],
      };
      data.billing.email = this.props.userData.email;
      console.log(data);
      var create_response = await services.restPostHandle("orders", data);
      console.log(create_response);
      this.setState({ addingToCart: false });
      Toast.show({
        text: "Your order has been placed",
        buttonText: "Okay",
        duration: 3000,
      });
      await AsyncStorage.removeItem("cart");
      await AsyncStorage.removeItem("orderDetails");
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: "Home",
            },
          ],
        })
      );
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
         <Header navigation={this.props.navigation} goBack={true}/>
        <ScrollView>
          <View>
            <View
              style={{
                height: 50,
                backgroundColor: "#e0e0e0",
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <Text style={{ marginLeft: 12, fontWeight: "bold", flex: 1 }}>
                {this.state.count} {this.props.appContent.item_in_my_bag}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.getCart();
                }}
              >
                <EvilIcons
                  name="refresh"
                  style={{ color: "#000000", marginRight: 12, fontSize: 30 }}
                />
              </TouchableOpacity>
            </View>

            <ScrollView>
              {this.state.isLoading ? (
                <Spinner />
              ) : this.state.isEmpty ? (
                <View
                  style={{
                    margin: 12,
                    alignItems: "center",
                    justifyContent: "center",
                    paddingTop: 100,
                    paddingBottom: 100,
                  }}
                >
                  <AntDesign
                    name="shoppingcart"
                    style={{ fontSize: 80, color: "#00000080" }}
                  />
                  <Text style={{ color: "#00000097" }}>
                    {this.props.appContent.add_something_in_bag}
                  </Text>
                </View>
              ) : (
                this.state.products.map((item, i) => (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      margin: 12,
                    }}
                  >
                    <Image
                      style={{ height: 120, width: 120 }}
                      source={{ uri: item.image }}
                    ></Image>

                    <View style={{ marginLeft: 6 }}>
                      <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text>
                          <Text>AED</Text>{" "}
                          <Text style={{ fontWeight: "bold", fontSize: 20 }}>
                            {item.cost}
                          </Text>
                        </Text>
                        <Text>
                          {item.name
                            ? item.name.substring(0, 20)
                            : "Product Name"}
                        </Text>
                      </View>
                      <View style={{ flexDirection: "row" }}>
                        <TouchableOpacity
                          onPress={() => {
                            if (item.quantity != 1) {
                              this.addToCart(
                                item.product_id,
                                item.quantity - 1
                              );
                            } else {
                              Toast.show({
                                text: "Select something larger",
                                buttonText: "Okay",
                                duration: 2000,
                              });
                            }
                          }}
                        >
                          <View
                            style={{
                              backgroundColor: "#e0e0e0",
                              height: 28,
                              borderRadius: 14,
                              width: 28,
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <AntDesign
                              name="minus"
                              size={18}
                              style={{ marginTop: 4 }}
                            />
                          </View>
                        </TouchableOpacity>
                        <View
                          style={{
                            backgroundColor: "#000",
                            height: 28,
                            borderRadius: 14,
                            width: 28,
                            justifyContent: "center",
                            alignItems: "center",
                            marginLeft: 6,
                          }}
                        >
                          <Text style={{ fontSize: 16, color: "#fff" }}>
                            {item.quantity.toString()}
                          </Text>
                        </View>
                        <TouchableOpacity
                          style={{ marginLeft: 6 }}
                          onPress={() => {
                            this.addToCart(item.product_id, item.quantity + 1);
                          }}
                        >
                          <View
                            style={{
                              backgroundColor: "#e0e0e0",
                              height: 28,
                              borderRadius: 14,
                              width: 28,
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <AntDesign
                              name="plus"
                              size={18}
                              style={{ marginTop: 4 }}
                            />
                          </View>
                        </TouchableOpacity>
                        <Text
                          style={{ marginTop: 4, fontSize: 18, marginLeft: 6 }}
                        >
                          {this.props.appContent.quantity}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View style={{ flex: 1 }}></View>
                      <View>
                        <TouchableOpacity
                          onPress={() => {
                            this.removeCart(item.product_id);
                          }}
                        >
                          <AntDesign
                            name="delete"
                            size={26}
                            style={{ marginTop: 4, marginLeft: 12 }}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ))
              )}

              <View
                style={{
                  flexDirection: "row",
                  height: 25,
                  alignItems: "center",
                  backgroundColor: "#FFC0CB40",
                  paddingRight: 12,
                  paddingLeft: 12,
                  marginTop: 16,
                }}
              >
                <Text style={{ flex: 1, fontWeight: "bold" }}>
                  Your Saved Address
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Checkout");
                  }}
                >
                  <Text>Edit</Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: "row",

                  alignItems: "center",
                  backgroundColor: "#FFC0CB40",
                  paddingRight: 12,
                  paddingLeft: 12,
                }}
              >
                <View>
                  <Text>
                    {this.props.userData?.first_name}{" "}
                    {this.props.userData?.last_name}
                  </Text>
                  <Text>
                    {this.props.userData?.shipping.address_1}{" "}
                    {this.props.userData?.shipping.address_2}
                  </Text>
                  <Text>{this.props.userData?.shipping.city}</Text>
                  <Text>{this.props.userData?.shipping.country}</Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: "row",

                  alignItems: "center",
                  backgroundColor: "#FFC0CB40",
                  paddingRight: 12,
                  paddingLeft: 12,
                  marginTop: 16,
                }}
              >
                <Text style={{ flex: 1, fontWeight: "bold" }}>
                  Items Subtotal
                </Text>
                <Text style={{ fontSize: 18 }}>
                  AED {this.state.netTotal.toString()}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  height: 25,
                  alignItems: "center",
                  backgroundColor: "#FFC0CB40",
                  paddingRight: 12,
                  paddingLeft: 12,
                }}
              >
                <Text style={{ flex: 1, fontWeight: "bold" }}>Shipping</Text>
                <Text style={{ fontSize: 18 }}>
                  AED{" "}
                  {this.state.shippingTotal ? this.state.shippingTotal : "0"}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  height: 25,
                  alignItems: "center",
                  backgroundColor: "#FFC0CB40",
                  paddingRight: 12,
                  paddingLeft: 12,
                }}
              >
                <Text style={{ flex: 1, fontWeight: "bold" }}>Order Total</Text>
                <Text style={{ fontSize: 18 }}>
                  AED {this.state.grandTotal.toString()}
                </Text>
              </View>
              <TouchableOpacity
                style={{ margin: 24 }}
                disabled={this.state.isEmpty ? true : false}
                onPress={this.checkoutCard}
              >
                <View
                  style={{
                    backgroundColor: this.state.isEmpty
                      ? "#00000090"
                      : "#000000",
                    padding: 12,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {this.state.addingToCart ? (
                    <Spinner color="#fff" style={{ color: "#fff" }} />
                  ) : (
                    <Text style={{ color: "#fff", fontWeight: "bold" }}>
                      {this.props.appContent.checkout} by Card
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ margin: 24, marginTop: 0 }}
                disabled={this.state.isEmpty ? true : false}
                onPress={this.checkout}
              >
                <View
                  style={{
                    backgroundColor: this.state.isEmpty
                      ? "#00000090"
                      : "#000000",
                    padding: 12,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {this.state.addingToCart ? (
                    <Spinner color="#fff" style={{ color: "#fff" }} />
                  ) : (
                    <Text style={{ color: "#fff", fontWeight: "bold" }}>
                      Cash on Delivery
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <Footer navigation={this.props.navigation} />
        </ScrollView>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.paymentModal}
          onRequestClose={() => {
            this.setState({ paymentModal: false });
          }}
        >
          <View
            style={{
              backgroundColor: "#00000090",
              justifyContent: "center",
              alignItems: "center",
              flex: 1,
            }}
          >
            <View
              style={{
                height: 200,
                width: "80%",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#fff",
              }}
            >
              <Text>Please wait</Text>
              <Spinner color="#000" style={{ color: "#000" }} />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const userData = state.userData;
  const appContent = state.appContent;
  const appContentType = state.appContentType;

  return { appContent, appContentType, userData };
};

export default connect(mapStateToProps)(Cart);
