import { INCREMENT_COUNTER, CART_COUNT, USER_DATA, APP_CONTENT, APP_CONTENT_TYPE } from "./action";
import English from '../constants/Languages/English';

const initialState = {
  counter: 0,
  cartCount: "",
  userData: null,
  appContent: English,
  appContentType: "EN"
};

const reducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case INCREMENT_COUNTER:
      return {
        ...state,
        counter: state.counter + 1,
      };
    case USER_DATA:
      return {
        ...state,
        userData: action.data,
      };
    case CART_COUNT:
      return {
        ...state,
        cartCount: action.data,
      };
    case APP_CONTENT:
      return {
        ...state,
        appContent: action.data,
      };
    case APP_CONTENT_TYPE:
      return {
        ...state,
        appContentType: action.data,
      };
    default:
      return state;
  }
};

export default reducer;
