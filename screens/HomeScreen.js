import * as React from "react";
import {
  View,
  ScrollView,
  Text,
  Image,
  ImageBackground,
  Dimensions,
} from "react-native";
import Header from "./Header";
import Footer from "./Footer";
import { DeckSwiper, Spinner, Icon } from "native-base";
import { TouchableOpacity, TextInput } from "react-native-gesture-handler";
import axios from "axios";
import { func } from "prop-types";
import { CommonActions } from "@react-navigation/native";
import Gallery from "react-native-image-gallery";
import { SliderBox } from "react-native-image-slider-box";
import { userData, cartCount } from "../redux-store/action";
import { connect } from "react-redux";
import services from "../services";

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slider: [],
      products: {},
      isLoading: true,
      sliderLoad: false,
      featured_brands: [],
      top_category: [],
      columnsLoad: true,
      columnsLoad3: {},
      banner1: {},
      banner2: {},
      banner3: {},
      featured_products: [],
      our_selection: [],
      banner: [],
      subbanner: {},
      social: [],
    };
    this.getTopCategories();
    this.getFeaturedProducts();
    this.getOurSelection();
    this.getBanners();
  }

  componentDidMount() {
    this.setState({ isLoading: true });
  }

  getBanners = async () => {
    var response = await services.restGetHandleApi("banners");
    var arr = [];
    response.forEach((element) => {
      arr.push({
        uri: "https://apis.hadoro.ae/uploads/banners/" + element.value,
      });
    });
    this.setState({ banner: arr });

    var subbanner = await services.restGetHandleApi("subbanners");
    this.setState({ subbanner });
  };

  onLayout = (e) => {
    this.setState({
      width: e.nativeEvent.layout.width,
    });
  };

  getTopCategories = async () => {
    var response = await services.restGetHandleParams(
      "products/categories?parent=102"
    );
    this.setState({ top_category: response });
  };

  getFeaturedProducts = async () => {
    var response = await services.restGetHandleParams("products?category=100&per_page=4");
    this.setState({ featured_products: response });
    this.setState({ isLoading: false });
  };

  getOurSelection = async () => {
    // var response = await services.restGetHandleParams("products?category=101");
    // this.setState({ our_selection: response });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
         <Header navigation={this.props.navigation}/>
        <ScrollView>
          {this.state.sliderLoad ? (
            <Spinner color="#000" />
          ) : (
            <SliderBox
              images={this.state.banner}
              onCurrentImagePressed={(index) =>
                console.warn(`image ${index} pressed`)
              }
              dotColor="#000"
              inactiveDotColor="#90A4AE"
              sliderBoxHeight={400}
            />
          )}
          <View style={{ flexDirection: "row", margin: 12 }}>
            <ImageBackground
              style={{ height: 280, flex: 1, margin: 12 }}
              source={{
                uri:
                  "https://apis.hadoro.ae/uploads/banners/" +
                  this.state.subbanner?.left?.value,
              }}
              resizeMode="contain"
            ></ImageBackground>
            <ImageBackground
              style={{ height: 280, flex: 1, margin: 12 }}
              source={{
                uri:
                  "https://apis.hadoro.ae/uploads/banners/" +
                  this.state.subbanner?.right?.value,
              }}
              resizeMode="contain"
            ></ImageBackground>
          </View>
          <Text
            style={{
              marginTop: 12,
              width: "100%",
              textAlign: "center",
              fontWeight: "bold",
              fontSize: 16,
            }}
          >
            {this.state.isLoading
              ? null
              : this.props.appContent.featured_products}
          </Text>
          <ScrollView
            horizontal={true}
            style={{
              flexWrap: "wrap",
              flexDirecton: "row",
              marginTop: 12,
            }}
          >
            {this.state.isLoading
              ? null
              : this.state.featured_products.map((item) => (
                  <View
                    style={{
                      flexBasis: "50%",
                      margin: 12,
                      width: Dimensions.get("window").width / 2.3,
                    }}
                    key={item.id.toString()}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate("Products", {
                          id: item.id,
                        });
                      }}
                    >
                      {item.images.length > 0 ? (
                        <Image
                          style={{ height: 150, width: "100%", backgroundColor:"#fff" }}
                          source={{ uri: item.images[0].src }}
                          resizeMode="contain"
                        ></Image>
                      ) : (
                        <View style={{ height: 150, width: "100%" }}></View>
                      )}
                    </TouchableOpacity>
                    <Text style={{ color: "grey", marginTop: 6 }}>
                      {this.props.appContentType == "EN"
                        ? item.name
                          ? item.name.substr(0, 20)
                          : null
                        : item.arabic
                        ? item.arabic.name.substr(0, 20)
                        : null}
                    </Text>
                    {/* <Text style={{ fontSize: 12, marginTop: 6, height: 70 }}>
                      {this.props.appContentType == "EN"
                        ? item.short_description
                        : item.arabic
                        ? item.arabic.short_description
                        : ""}
                    </Text> */}
                    <View
                      style={{
                        height: 1,
                        width: "100%",
                        backgroundColor: "grey",
                        marginTop: 6,
                        marginBottom: 6,
                      }}
                    />
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          fontSize: 16,
                          fontWeight: "bold",
                          marginLeft: 8,
                        }}
                      >
                        AED {item.price}
                      </Text>
                    </View>
                  </View>
                ))}
          </ScrollView>

{/* OUR SELECTION DISABLED */}
          {/* <Text
            style={{
              marginTop: 12,
              width: "100%",
              textAlign: "center",
              fontWeight: "bold",
              fontSize: 16,
            }}
          >
            {this.state.isLoading ? null : this.props.appContent.our_selection}
          </Text>

          <ScrollView
            horizontal={true}
            style={{
              flexWrap: "wrap",
              flexDirecton: "row",
              marginTop: 12,
            }}
          >
            {this.state.isLoading
              ? null
              : this.state.our_selection.map((item) => (
                  <View
                    style={{
                      flexBasis: "50%",
                      margin: 12,
                      width: Dimensions.get("window").width / 2.3,
                    }}
                    key={item.id.toString()}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate("Products", {
                          id: item.id,
                        });
                      }}
                    >
                      {item.images.length > 0 ? (
                        <Image
                          style={{ height: 150, width: "100%" }}
                          source={{ uri: item.images[0].src }}
                        ></Image>
                      ) : (
                        <View style={{ height: 150, width: "100%" }}></View>
                      )}
                    </TouchableOpacity>
                    <Text style={{ color: "grey", marginTop: 6 }}>
                      {this.props.appContentType == "EN"
                        ? item.name
                          ? item.name.substr(0, 20)
                          : null
                        : item.arabic
                        ? item.arabic.name.substr(0, 20)
                        : null}
                    </Text>
                    <View
                      style={{
                        height: 1,
                        width: "100%",
                        backgroundColor: "grey",
                        marginTop: 6,
                        marginBottom: 6,
                      }}
                    />
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          fontSize: 16,
                          fontWeight: "bold",
                          marginLeft: 8,
                        }}
                      >
                        AED {item.price}
                      </Text>
                    </View>
                  </View>
                ))}
          </ScrollView> */}

          <Text
            style={{
              marginTop: 16,
              width: "100%",
              textAlign: "center",
              fontWeight: "bold",
              fontSize: 16,
              marginBottom: 4,
            }}
          >
            {this.state.top_category?.length == 0
              ? null
              : this.props.appContent.top_categories}
          </Text>
          {this.state.top_category?.length == 0
              ? null : this.state.top_category.map((item, i) => (
            <ImageBackground
              style={{ height: 350, flex: 1, margin: 12, alignItems: "center" }}
              source={
                item.image
                  ? { uri: item.image.src }
                  : require("../assets/images/no_img.png")
              }
              key={i.toString()}
              resizeMode="cover"
            >
              <TouchableOpacity
                style={{
                  marginTop: 300,
                  width: Dimensions.get("window").width - 24,
                  flex: 1,
                }}
                onPress={() => {
                  this.props.navigation.navigate("GetProducts", {
                    id: item.id,
                    type: "category",
                    name:
                      this.props.appContentType == "EN"
                        ? item.name
                        : item.arabic
                        ? item.arabic.name
                        : "",
                    parent_id: null,
                  });
                }}
              >
                <View
                  style={{
                    backgroundColor: "#000",
                    padding: 8,
                    width: Dimensions.get("window").width - 24,
                    justifyContent: "center",
                    alignItems: "center",
                    height: 50,
                  }}
                >
                  <Text style={{ color: "#fff", fontWeight: "bold" }}>
                    {this.props.appContentType == "EN"
                      ? item.name
                      : item.arabic
                      ? item.arabic.name
                      : ""}
                  </Text>
                </View>
              </TouchableOpacity>
            </ImageBackground>
          ))}
          <Footer navigation={this.props.navigation} />
        </ScrollView>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  header: null,
};

const mapStateToProps = (state) => {
  const cartCount = state.cartCount;
  const userData = state.userData;
  const appContent = state.appContent;
  const appContentType = state.appContentType;
  return { cartCount, userData, appContent, appContentType };
};

export default connect(mapStateToProps)(HomeScreen);
