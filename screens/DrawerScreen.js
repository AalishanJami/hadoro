import React, { Component } from "react";
import { View } from "react-native";
import DrawerRender from "../navigation/DrawerRender";

function DrawerScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <DrawerRender navigation={props.navigation} />
    </View>
  );
}

export default DrawerScreen;
