import * as React from "react";
import {
  View,
  ScrollView,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import Header from "./Header";
import { TextInput } from "react-native-gesture-handler";
import Footer from "./Footer";
import Login from "./Login";
import Register from "./Register";
import { AntDesign } from '@expo/vector-icons'; 

class Auth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "login",
    };
  }

  componentDidMount() {}

  render() {
    return (
      <View style={{ flex: 1 }}>
         <Header navigation={this.props.navigation} goBack={true}/>
        <View style={{ flexDirection: "row", padding: 12 }}>
          <TouchableOpacity style={{ flex: 1, marginTop: 12, alignItems:"center" }} onPress={()=>{
              this.setState({selected: "register"})
          }}>
            <View
              style={{
                paddingBottom: 24,
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                borderBottomWidth: 1,
                borderBottomColor:
                  this.state.selected == "register" ? "#000" : "#ccc",
              }}
            >
              <Text
                style={{
                  fontSize: 22,
                  fontWeight: "bold",
                  color: this.state.selected == "register" ? "#000" : "#ccc",
                  marginTop: 12,
                  marginBottom: 12,
                }}
              >
               {this.props.appContent.im_new_here}
              </Text>
            </View>
            {this.state.selected == "register" ?
            <AntDesign name="caretdown" size={24} color="black" style={{marginTop:-8}}/> : null}
          </TouchableOpacity>
         
          <TouchableOpacity style={{ flex: 1 , marginTop: 12, alignItems:"center"}} onPress={()=>{
              this.setState({selected: "login"})
          }}>
            <View
              style={{
                paddingBottom: 24,
                justifyContent: "center",
                alignItems: "center",
                width:"100%",
                borderBottomWidth: 1,
                borderBottomColor:
                  this.state.selected == "login" ? "#000" : "#ccc",
              }}
            >
              <Text
                style={{
                  fontSize: 22,
                  fontWeight: "bold",
                  color: this.state.selected == "login" ? "#000" : "#ccc",
                  marginTop: 12,
                  marginBottom: 12,
                }}
              >
                {this.props.appContent.been_here_before}
              </Text>
            </View>
            {this.state.selected == "login" ?
            <AntDesign name="caretdown" size={24} color="black" style={{marginTop:-8}} /> : null}
          </TouchableOpacity>
         
        </View>
        <ScrollView>
          {this.state.selected == "login" ? (
            <Login navigation={this.props.navigation} />
          ) : (
            <Register  navigation={this.props.navigation} />
          )}
          <Footer navigation={this.props.navigation} />
        </ScrollView>
      </View>
    );
  }
}

Auth.navigationOptions = {
  header: null,
};
import {connect} from 'react-redux'; 
const mapStateToProps = (state) => {
 
 const appContent = state.appContent;
const appContentType = state.appContentType;

  return { appContent , appContentType};
};

export default connect(mapStateToProps)(Auth);

