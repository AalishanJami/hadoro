import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  Dimensions,
  ScrollView,
} from "react-native";
import Footer from "./Footer";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import Header from "./Header";
import axios from "axios";
import services from "../services";
class GetProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: false,
      password: "",
      searchText: "",
      count: 0,
      isEmpty: true,
      products: [],
      name: "",
    };
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener("focus", () => {
     this.publish()
    });
    this.publish();
  }

  publish = async () => {
    var type = this.props.route.params.type;
    this.setState({ name: this.props.route.params.name });
    if (type == "brands") {
      this.brands(this.props.route.params.id);
    } else if (type == "category") {
      this.category(
        this.props.route.params.id,
        this.props.route.params.parent_id
      );
    }
  }
  componentWillUnmount() {
    this._unsubscribe();
  }

  async componentDidUpdate(prevProps){
    if(this.props.route.params.name != prevProps.route.params.name){
     this.publish()
    }
  }

  brands = async (id) => {
    var self = this;
    axios
      .get("https://phone7m.shop/sys/public/api/product/brand/" + id)
      .then(function (response) {
        console.log(response.data);
        if (response.data.success) {
          self.setState({ isEmpty: false });
          self.setState({ products: response.data.data });
          self.setState({ count: response.data.data.length });
        } else {
          self.setState({ isEmpty: true });
          self.setState({ count: 0 });
          self.setState({ products: [] });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  category = async (id) => {
    var response = await services.restGetHandleParams(
      "products?category=" + id
    );
    if (response.length > 0) {
      this.setState({ isEmpty: false });
      this.setState({ products: response });
      this.setState({ count: response.length });
    } else {
      this.setState({ isEmpty: true });
      this.setState({ count: 0 });
      this.setState({ products: [] });
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
         <Header navigation={this.props.navigation} goBack={true}/>
        <ScrollView>
          <View style={{ marginBottom: 24 }}>
            <Text
              style={{
                alignSelf: "center",
                margin: 20,
                fontSize: 18,
                fontWeight: "bold",
              }}
            >
              {this.props.appContent.display_products_from} {this.state.name}
            </Text>
            <View
              style={{
                height: 50,
                borderBottomColor: "#808080",
                justifyContent: "center",
                borderBottomWidth: 1,
                marginLeft: 12,
                marginRight: 12,
              }}
            >
              <Text style={{ marginLeft: 12, color: "#808080" }}>
                {this.state.count.toString()}{" "}
                {this.props.appContent.products_fount}
              </Text>
            </View>
            <ScrollView>
              <View
                style={{
                  flex: 1,
                  flexWrap: "wrap",
                  flexDirection: "row",
                  marginTop: 12,
                }}
              >
                {this.state.products.map((item) => (
                  <View
                    style={{
                      flex: 1,
                      flexBasis: "40%",
                      backgroundColor: "#fff",
                      margin: 6,
                      borderColor: "#ccc",
                      borderWidth: 1,
                      padding: 12,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate("Products", {
                          id: item.id,
                        });
                      }}
                    >
                      {item.images.length > 0 ? (
                        <Image
                          style={{ height: 150, width: "100%" }}
                          source={{ uri: item.images[0].src }}
                        ></Image>
                      ) : (
                        <View style={{ height: 150, width: "100%" }} />
                      )}
                    </TouchableOpacity>

                    <Text style={{ color: "grey", marginTop: 6 }}>
                      {this.props.appContentType == "EN"
                        ? item.name
                        : item.arabic
                        ? item.arabic.name
                        : ""}
                    </Text>
                    <View
                      style={{
                        height: 1,
                        width: "100%",
                        backgroundColor: "grey",
                        marginTop: 6,
                        marginBottom: 6,
                      }}
                    />
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ fontSize: 14 }}>AED {item.price}</Text>
                      {/* <Text
                      style={{
                        fontSize: 16,
                        fontWeight: "bold",
                        marginLeft: 8,
                      }}
                    >
                      6,000
                    </Text> */}
                    </View>
                  </View>
                ))}
              </View>
              {this.state.isEmpty ? (
                <View
                  style={{
                    margin: 12,
                    alignItems: "center",
                    justifyContent: "center",
                    paddingTop: 100,
                    paddingBottom: 100,
                  }}
                >
                  <AntDesign
                    name="search1"
                    style={{ fontSize: 80, color: "#00000080" }}
                  />
                  <Text style={{ color: "#00000097", marginTop: 24 }}>
                    {this.props.appContent.you_will_find_something}
                  </Text>
                </View>
              ) : null}
            </ScrollView>
          </View>

          <Footer navigation={this.props.navigation} />
        </ScrollView>
      </View>
    );
  }
}

import { connect } from "react-redux";
const mapStateToProps = (state) => {
  const appContent = state.appContent;
  const appContentType = state.appContentType;

  return { appContent, appContentType };
};

export default connect(mapStateToProps)(GetProducts);
