import React, { Component } from "react";
import { View, Image, TextInput, Text } from "react-native";
import * as Linking from "expo-linking";
import { EvilIcons, AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import axios from "axios";
import { func } from "prop-types";
import { Spinner, Toast } from "native-base";
import { connect } from "react-redux";
import services from "../services";

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      askAuth: false,
      searchText: "",
      contacts: {},
      subscribe: "",
      social: [],
    };
  }

  async componentDidMount() {
    this.get();
  }

  get = async () => {
    var response = await services.restGetHandleApi("social");
    this.setState({ social: response });
  };

  subscribe = () => {
    Toast.show({
      text: "Subscribed",
      buttonText: "Okay",
      duration: 3000,
    });
    this.setState({ awaitingSub: false });
  };

  render() {
    return (
      <View style={{ backgroundColor: "#404040", width: "100%", padding: 24 }}>
        <Text
          style={{
            color: "#fff",
            fontSize: 18,
            fontWeight: "bold",
            alignSelf: "center",
          }}
        >
          {this.props.appContent.join_us}
        </Text>
        <TextInput
          value={this.state.subscribe}
          onChangeText={(text) => this.setState({ subscribe: text })}
          placeholder={this.props.appContent.enter_email_for_latest_updates}
          style={{
            width: "100%",
            backgroundColor: "#fff",
            padding: 12,
            marginTop: 24,
          }}
        />
        <View
          style={{
            backgroundColor: this.state.awaitingSub ? "#00000000" : "#000000",
            padding: 12,
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            marginTop: 8,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              if (this.state.subscribe != "") {
                this.setState({ awaitingSub: true });
                setTimeout(this.subscribe, 1500);
              } else {
                Toast.show({
                  text: "Write an email!",
                  buttonText: "Okay",
                  duration: 3000,
                });
              }
            }}
          >
            {this.state.awaitingSub ? (
              <Spinner color="#ffff" />
            ) : (
              <Text style={{ color: "#fff", fontWeight: "bold" }}>
                {this.props.appContent.subscribe}
              </Text>
            )}
          </TouchableOpacity>
        </View>

        <View
          style={{
            backgroundColor: "#666",
            width: "100%",
            marginTop: 36,
            height: 1,
          }}
        />

        <Text
          style={{
            color: "#fff",
            alignSelf: "center",
            fontWeight: "bold",
            fontSize: 20,
            marginTop: 16,
          }}
        >
          {this.props.appContent.connect_with_us}
        </Text>
        <View
          style={{ alignSelf: "center", flexDirection: "row", marginTop: 12 }}
        >
          {this.state.social.map((item) =>
            item.name === "facebook" ? (
              <TouchableOpacity
                style={{ margin: 6 }}
                onPress={() => {
                  Linking.openURL(item.value);
                }}
              >
                <Entypo
                  name="facebook"
                  style={{ color: "#fff", fontSize: 20 }}
                />
              </TouchableOpacity>
            ) : item.name === "twitter" ? (
              <TouchableOpacity
                style={{ margin: 6 }}
                onPress={() => {
                  Linking.openURL(item.value);
                }}
              >
                <Entypo
                  name="twitter"
                  style={{ color: "#fff", fontSize: 20 }}
                />
              </TouchableOpacity>
            ) : item.name === "instagram" ? (
              <TouchableOpacity
                style={{ margin: 6 }}
                onPress={() => {
                  Linking.openURL(item.value);
                }}
              >
                <Entypo
                  name="instagram"
                  style={{ color: "#fff", fontSize: 20 }}
                />
              </TouchableOpacity>
            ) : item.name === "youtube" ? (
              <TouchableOpacity
                style={{ margin: 6 }}
                onPress={() => {
                  Linking.openURL("https://www.youtube.com");
                }}
              >
                <Entypo
                  name="youtube"
                  style={{ color: "#fff", fontSize: 20 }}
                />
              </TouchableOpacity>
            ) : null
          )}
        </View>

        {/* <View
          style={{
            backgroundColor: "#666",
            width: "100%",
            marginTop: 30,
            height: 1,
          }}
        /> */}

        {/* <Text
          style={{
            color: "#fff",
            alignSelf: "center",
            fontWeight: "bold",
            fontSize: 20,
            marginTop: 16,
          }}
        >
          {this.props.appContent.about_us}
        </Text> */}
        {/* <View style={{ alignSelf: "center", marginTop: 12 }}>
          <Text style={{ color: "#ffffff96" }}>
            {this.state.contacts.name}{" "}
            {this.state.contacts.tagline
              ? " - " + this.state.contacts.tagline
              : null}
          </Text>
          <Text style={{ color: "#ffffff96" }}>
            {this.state.contacts.email}
          </Text>
          <Text style={{ color: "#ffffff96" }}>
            {this.state.contacts.phone}
          </Text>
          <Text style={{ color: "#ffffff96" }}>
            {this.state.contacts.address_1 +
              " " +
              this.state.contacts.address_2}
          </Text>
          <Text style={{ color: "#ffffff96" }}>{this.state.contacts.city}</Text>
        </View>
      */}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const appContent = state.appContent;
  const appContentType = state.appContentType;

  return { appContent, appContentType };
};

export default connect(mapStateToProps)(Footer);
