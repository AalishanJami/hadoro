import * as React from "react";
import {
  View,
  ScrollView,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  AsyncStorage,
  Modal,
} from "react-native";
import Header from "./Header";
import Footer from "./Footer";
import {
  DeckSwiper,
  Spinner,
  Toast,
  Icon,
  Tab,
  Tabs,
  ScrollableTab,
} from "native-base";
import ImageViewer from "react-native-image-zoom-viewer";
import { TouchableOpacity, TextInput } from "react-native-gesture-handler";
import axios from "axios";
import AutoHeightWebView from "react-native-autoheight-webview";
import { AntDesign } from "@expo/vector-icons";
import { SliderBox } from "react-native-image-slider-box";
import { userData, cartCount } from "../redux-store/action";
import { connect } from "react-redux";
import services from "../services";
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slider: [],
      product: {},
      isLoading: true,
      quantity: 1,
      userData: {},
      addingToCart: false,
      modal: false,
      modalImage: "",
      caseColor: "",
      gripColor: "",
      variations: [],
      product_id: "",
      attributes: {},
      variation_id: "",
      attributeNames: {},
      preview_image: [],
      show_preview_image: false,
    };
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", () => {
      this.getProduct(this.props.route.params.id);
      this.getUser();
      this.getVariations(this.props.route.params.id);
    });
  }

  getUser = async () => {
    try {
      const value = await AsyncStorage.getItem("userData");
      console.log(value);
      if (value !== null) {
        this.setState({ userData: JSON.parse(value) });
      }
    } catch (error) {
      console.log(error);
    }
  };

  getProduct = async (id) => {
    this.setState({ isLoading: true });
    var response = await services.restGetHandle("products/" + id);
    var slider = [];
    response.images.forEach((element, i) => {
      slider.push(element.src);
    });
    this.setState({ product: response });
    this.setState({ slider });
    var caseColor = response.default_attributes[0]["option"];
    var gripColor = response.default_attributes[1]["option"];
    console.log(caseColor);
    console.log("case color");
    this.setState({ caseColor });
    this.setState({ gripColor });
  };

  getVariations = async (id) => {
    this.setState({ isLoading: true });
    var response = await services.restGetHandleParams(
      "products/" + id + "/variations?per_page=100"
    );
    var combined = [];
    if (response.length == 100) {
      let response2 = await services.restGetHandleParams(
        "products/" + id + "/variations?page=2&per_page=100"
      );
      if (response2.length > 0) {
        combined = [].concat(response, response2);
      }
    }
    console.log("products/" + id + "/variations");
    this.setState({ variations: combined.length > 0 ? combined : response });
    this.setState({ isLoading: false });
  };

  handleVariation = async () => {
    var caseColor = this.state.caseColor;
    var gripColor = this.state.gripColor;
    console.log("case color");
    console.log(caseColor);

    console.log("grip color");
    console.log(gripColor);
    var variation = this.state.variations;
    console.log(variation);
    var array = [];
    if (caseColor) {
      array.push({
        id: 1,
        name: "Case Color",
        option: caseColor,
      });
    }
    if (gripColor) {
      array.push({
        id: 2,
        name: "Grip Color",
        option: gripColor,
      });
    }
    variation.forEach((element, i) => {
      if (JSON.stringify(element.attributes) == JSON.stringify(array)) {
        console.log(element);
        var slider = [];
        slider.push(element.image.src);
        this.setState({ slider });
        this.setState({ variation_id: element.id });
      }
    });
  };

  addToCart = async () => {
    if (this.props.userData) {
      if (!this.state.variation_id) {
        Toast.show({
          text: "Please select variation(s) first",
          buttonText: "Okay",
          duration: 3000,
        });
        return;
      } else {
        this.setState({ addingToCart: true });
        var cart = [];
        try {
          cart = await AsyncStorage.getItem("cart");
          if (cart == null) {
            cart = [];
          } else {
            cart = await JSON.parse(cart);
          }
          console.log(cart);
        } catch (error) {
          console.log(error);
        }
        var product = {
          product_id: this.props.route.params.id,
          quantity: this.state.quantity,
          image: this.state.slider[0],
          name: this.state.product.name,
          cost: this.state.product.price.toString(),
          variation_id: this.state.variation_id,
        };
        console.log(product);
        if (cart?.length > 0) {
          var index = null;
          cart.forEach((element, i) => {
            console.log(element.product_id, this.props.route.params.id);
            if (element.product_id == this.props.route.params.id) {
              index = i;
              console.log(i);
            }
          });
          if (index != null) {
            cart[index] = product;
          } else {
            cart.push(product);
          }
        } else {
          cart = [];
          cart.push(product);
        }
        console.log(cart);
        await AsyncStorage.setItem("cart", JSON.stringify(cart));
        await this.props.dispatch(cartCount(cart.length.toString()));
        Toast.show({
          text: "Product added to cart successfully",
          buttonText: "Okay",
          duration: 3000,
        });
      }
    } else {
      Toast.show({
        text: "Please login to proceed",
        buttonText: "Okay",
        duration: 3000,
      });
      this.props.navigation.navigate("Auth");
    }
    this.setState({ addingToCart: false });
    return;
  };

  render() {
    const product = this.state.product;
    return (
      <View style={{ flex: 1 }}>
        <Header navigation={this.props.navigation} goBack={true} />
        {this.state.isLoading ? (
          <Spinner color="#000" />
        ) : (
          <ScrollView style={{ flex: 1 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.show_preview_image}
              onRequestClose={() => {
                this.setState({ show_preview_image: false });
              }}
            >

                <View
                  style={{
                    backgroundColor: "#F5FCFF",
                    flex: 1,
                  }}
                >
                 <TouchableOpacity
                  style={{backgroundColor:"#000", paddingTop: 36, paddingLeft: 12 }}
                      onPress={() =>
                        this.setState({
                          preview_image: [],
                          show_preview_image: false,
                        })
                      }
                    >
                      <AntDesign
                        name="arrowleft"
                        style={{ fontSize: 30, color: "#fff" }}
                      />
                    </TouchableOpacity>
                  <ImageViewer
                    imageUrls={this.state.preview_image}
                    renderIndicator={() => null}
                  />
                </View>
            </Modal>
            <SliderBox
              images={this.state.slider}
              sliderBoxHeight={380}
              onCurrentImagePressed={(index) => {
                console.warn(`image ${index} pressed`);
                console.log(this.state.slider[index]);
                if (index == 0 || index > 0) {
                  this.setState({
                    preview_image: [
                      {
                        url: this.state.slider[index],
                      },
                    ],
                    show_preview_image: true,
                  });
                }
              }}
              dotColor="#000"
              inactiveDotColor="#90A4AE"
            />
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modal}
              onRequestClose={() => {
                this.setState({ modal: false });
              }}
            >
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View style={{ height: height, width: width }}>
                  {this.state.modalImage ? (
                    <Image
                      source={{ uri: this.state.modalImage }}
                      style={{ height: height, width: width }}
                      resizeMode="contain"
                    ></Image>
                  ) : null}
                  <View
                    style={{
                      position: "absolute",
                      right: 4,
                      top: 4,
                      marginTop: 50,
                      marginRight: 20,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => this.setState({ modal: false })}
                    >
                      <Icon name="md-close" />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
            <View
              style={{ marginTop: 12, padding: 12, backgroundColor: "#fff" }}
            >
              {/* <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#808080" }}>
                  {this.props.appContent.brand}
                </Text>
                <Text style={{ marginLeft: 12, color: "#808080" }}>
                  {this.props.appContentType == "EN"
                    ? product.brand.name.toUpperCase()
                    : product.brand.arabic
                    ? product.brand.arabic.name
                    : ""}
                </Text>
              </View> */}

              <Text style={{ fontSize: 22, marginTop: 12 }}>
                {this.props.appContentType == "EN"
                  ? product.name
                  : product.arabic
                  ? product.arabic.name
                  : ""}
              </Text>
              {product.attributes?.map((item, i) => (
                <View key={item.id.toString()}>
                  <Text
                    style={{
                      marginTop: 20,
                      fontWeight: "bold",
                      fontSize: 18,
                      alignSelf: "center",
                      marginBottom: 20,
                    }}
                  >
                    {item.name}
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    {item.options.map((color, i) => (
                      <TouchableOpacity
                        key={i.toString()}
                        onPress={() => {
                          var attributes = this.state;
                          var attributeNames = this.state;
                          attributeNames[item.id] = item.name;
                          attributes[item.id] = color;
                          if (item.name == "Case Color") {
                            this.setState({ caseColor: color });
                          } else {
                            this.setState({ gripColor: color });
                          }
                          this.setState({ attributes });
                          this.setState({ attributeNames }, () =>
                            this.handleVariation()
                          );
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: color.toLowerCase(),
                            height: 20,
                            width: 20,
                            borderRadius: 10,
                            marginLeft: 4,
                            marginRight: 4,
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          {this.state.attributes[item.id] &&
                          this.state.attributes[item.id] == color ? (
                            <Icon
                              name="check"
                              type="AntDesign"
                              style={{ color: "#fff", fontSize: 12 }}
                            />
                          ) : null}
                        </View>
                      </TouchableOpacity>
                    ))}
                  </View>
                </View>
              ))}
              <View style={{ marginTop: 24, flexDirection: "row" }}>
                <View
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    alignItems: "center",
                  }}
                >
                  <Text style={{ fontSize: 16 }}>AED</Text>
                  <Text
                    style={{ fontWeight: "bold", fontSize: 22, marginLeft: 6 }}
                  >
                    {product.price}
                  </Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.quantity != 1) {
                        this.setState(
                          { quantity: this.state.quantity - 1 },
                          () => {
                            this.addToCart();
                          }
                        );
                      } else {
                        Toast.show({
                          text: "Select something larger",
                          buttonText: "Okay",
                          duration: 2000,
                        });
                      }
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: "#80808050",
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text style={{ fontSize: 30 }}>-</Text>
                    </View>
                  </TouchableOpacity>
                  <View
                    style={{
                      backgroundColor: "#000",
                      height: 40,
                      width: 40,
                      borderRadius: 20,
                      alignItems: "center",
                      justifyContent: "center",
                      marginLeft: 12,
                      marginRight: 12,
                    }}
                  >
                    <Text style={{ fontSize: 20, color: "#fff" }}>
                      {this.state.quantity.toString()}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState(
                        { quantity: this.state.quantity + 1 },
                        () => {
                          this.addToCart();
                        }
                      );
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: "#80808050",
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text style={{ fontSize: 30 }}>+</Text>
                    </View>
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginLeft: 12,
                      alignSelf: "center",
                      fontSize: 18,
                    }}
                  >
                    {this.props.appContent.quantity}
                  </Text>
                </View>
              </View>
              <TouchableOpacity
                style={{ marginTop: 20 }}
                onPress={this.addToCart}
              >
                <View
                  style={{
                    backgroundColor: "#000000",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: 16,
                  }}
                >
                  {this.state.addingToCart ? (
                    <Spinner color="#fff" style={{ color: "#fff" }} />
                  ) : (
                    <Text
                      style={{
                        color: "#fff",
                        fontWeight: "bold",
                        fontSize: 18,
                      }}
                    >
                      {this.props.appContent.add_to_bag}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
              <Text
                style={{
                  marginTop: 20,
                  fontWeight: "bold",
                  fontSize: 18,
                  alignSelf: "center",
                  marginBottom: 20,
                }}
              >
                {this.props.appContent.product_details}
              </Text>

              <Tabs renderTabBar={() => <ScrollableTab />}>
                {this.props.appContentType == "EN" ? (
                  <Tab heading={this.props.appContent.overview}>
                    <AutoHeightWebView
                      style={{
                        width: Dimensions.get("window").width - 15,
                        marginTop: 35,
                      }}
                      // onSizeUpdated={(size) => console.log(size.height)}
                      source={{
                        html: product.description,
                      }}
                      scalesPageToFit={true}
                      viewportContent={"width=device-width, user-scalable=no"}
                    />
                  </Tab>
                ) : product.arabic ? (
                  <Tab heading={this.props.appContent.overview}>
                    <AutoHeightWebView
                      style={{
                        width: Dimensions.get("window").width - 15,
                        marginTop: 35,
                      }}
                      // onSizeUpdated={(size) => console.log(size.height)}
                      source={{ html: product.arabic.description }}
                      scalesPageToFit={true}
                      viewportContent={"width=device-width, user-scalable=no"}
                    />
                  </Tab>
                ) : (
                  <Tab heading={this.props.appContent.overview}></Tab>
                )}

                {/* {this.props.appContentType == "EN" ? (
                  <Tab heading={this.props.appContent.specification}>
                    <AutoHeightWebView
                      style={{
                        width: Dimensions.get("window").width - 15,
                        marginTop: 35,
                      }}
                      onSizeUpdated={(size) => console.log(size.height)}
                      source={{
                        html: product.specification,
                      }}
                      scalesPageToFit={true}
                      viewportContent={"width=device-width, user-scalable=no"}
                    />
                  </Tab>
                ) : product.arabic ? (
                  <Tab heading={this.props.appContent.specification}>
                    <AutoHeightWebView
                      style={{
                        width: Dimensions.get("window").width - 15,
                        marginTop: 35,
                      }}
                      onSizeUpdated={(size) => console.log(size.height)}
                      source={{ html: product.arabic.specification }}
                      scalesPageToFit={true}
                      viewportContent={"width=device-width, user-scalable=no"}
                    />
                  </Tab>
                ) : (
                  <Tab heading={this.props.appContent.specification}></Tab>
                )} */}
              </Tabs>
              {/* <Text
                style={{
                  marginTop: 20,
                  fontWeight: "bold",
                  fontSize: 18,
                  alignSelf: "center",
                  marginBottom: 20,
                }}
              >
                {this.props.appContent.related_products}
              </Text>
              <ScrollView
                horizontal={true}
                style={{
                  flexWrap: "wrap",
                  flexDirecton: "row",
                  marginTop: 12,
                }}
              >
                {product.related.length > 0
                  ? product.related.map((item) => (
                      <View
                        style={{
                          flexBasis: "50%",
                          margin: 12,
                          width: Dimensions.get("window").width / 2.3,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this.props.navigation.push("Products", {
                              id: item.id,
                            });
                          }}
                        >
                          <Image
                            style={{ height: 150, width: "100%" }}
                            source={{ uri: item.base_image.path }}
                          ></Image>
                        </TouchableOpacity>
                        <Text style={{ color: "grey", marginTop: 6 }}>
                          {this.props.appContentType == "EN"
                            ? item.name.substr(0, 20)
                            : item.arabic
                            ? item.arabic.name
                            : ""}
                        </Text>
                        <Text
                          style={{ fontSize: 12, marginTop: 6, height: 70 }}
                        >
                          {this.props.appContentType == "EN"
                            ? item.short_description
                            : item.arabic
                            ? item.arabic.short_description
                            : ""}
                        </Text>
                        <View
                          style={{
                            height: 1,
                            width: "100%",
                            backgroundColor: "grey",
                            marginTop: 6,
                            marginBottom: 6,
                          }}
                        />
                        <View style={{ flexDirection: "row" }}>
                          <Text
                            style={{
                              fontSize: 16,
                              fontWeight: "bold",
                              marginLeft: 8,
                            }}
                          >
                            {item.price.formatted}
                          </Text>
                        </View>
                      </View>
                    ))
                  : null}
              </ScrollView>
           
            */}
            </View>

            <Footer navigation={this.props.navigation} />
          </ScrollView>
        )}
      </View>
    );
  }
}

Products.navigationOptions = {
  header: null,
};

const mapStateToProps = (state) => {
  const cartCount = state.cartCount;
  const userData = state.userData;
  const appContent = state.appContent;
  const appContentType = state.appContentType;
  return { cartCount, userData, appContent, appContentType };
};

export default connect(mapStateToProps)(Products);
